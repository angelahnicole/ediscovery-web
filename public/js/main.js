/*
|--------------------------------------------------------------------------
| Author: Scott Halstvedt & Heather Shadoan
|--------------------------------------------------------------------------
*/
var container;
var camera, controls, scene, projector, renderer; // Three.js object initialization
var particles, geometry, material, i, h, color, colors = [], sprite, size;
var objects = [], plane; // Object intersection array (keeps track of objects to click on or mouse over in 3D)
var light = new THREE.SpotLight( 0xffffff, 1.5 ); // global light source


var mouse = new THREE.Vector2(),
        offset = new THREE.Vector3(),         //This is the new parameter for the outline
    INTERSECTED, LABEL_INTERSECTED, SELECTED, OUTLINE; // Mouse-over activation booleans

/*
| changeSelected - handles updating the graph with a new set of selected nodes from the API
*/
window.changeSelected = function(mod) {
    console.log("selected");
    window.selectedString = jQuery('input:checkbox:checked').map(function() { return this.id.substr(1); }).get().join();
    console.log(window.selectedString);
    window.refreshGraph();
}

function createView() {
    init();
    animate();
}

function doLights() {
    scene.add( new THREE.AmbientLight( 0x505050 ) );
    scene.add(light);
}

function init() {

	/*
	|--------------------------------------------------------------------------
	| Camera object
	|--------------------------------------------------------------------------
	| 
	| Add a camera object to the scene.
	|
	*/
    camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 1, 10000 );
    camera.position.z = 1000;
	
	/*
	|--------------------------------------------------------------------------
	| Scene creation
	|--------------------------------------------------------------------------
	| 
	| Create a scene for the world.
	|
	*/
    scene = new THREE.Scene();

	/*
	|--------------------------------------------------------------------------
	| Light creation
	|--------------------------------------------------------------------------
	| 
	| The next set of lines of code will create the lighting.
	|
	*/
    scene.add( new THREE.AmbientLight( 0x505050 ) );

    light.position.set( 0, 500, 2000 );
    light.castShadow = true;

    light.shadowCameraNear = 1;
    light.shadowCameraFar = camera.far;
    light.shadowCameraFov = 100;

    light.shadowBias = -0.0000022;
    light.shadowDarkness = 0.9;

    light.shadowMapWidth = 500;
    light.shadowMapHeight = 500;

    scene.add( light );
	
	/*
	|--------------------------------------------------------------------------
	| Author:Heather Shadoan
	| Sprite Logic
	|--------------------------------------------------------------------------
	|
	| This block of code will enable a graphic field of multi coloured sprites.
	| For the time being it will be commented out due to certain requirements.
	|
	|	geometry = new THREE.Geometry();
	|
	|	sprite = THREE.ImageUtils.loadTexture( "../public/images/ball.png" );
	|
	|	for ( i = 0; i < 750; i ++ ) {
	|
    |   var vertex = new THREE.Vector3();
	|
    |   var varx = 15000 * Math.random() - 7500;
	|	var vary = 15000 * Math.random() - 7500;
	|	var varz = 20000 * Math.random() - 10000;
	|
    |   	if((varz <= 2500 && varz >= 0) || (varz >= -2500 && varz <= -1))
    |   	{
    |      	 	 vertex.x = varx;
    |       	 vertex.y = vary;
    |       	 vertex.z = varz*10;
    |   	}
	|
    |  		 else
    |   	{
    |    		 vertex.x = varx;
    |      		 vertex.y = vary;
    |      		 vertex.z = varz;
    |   	}
	|
	|
    |       	 geometry.vertices.push( vertex );
	|
    |      	     colors[ i ] = new THREE.Color();
    |       	 colors[ i ].setHSL( Math.random(), 1, 0.5 );
    |	}
	|
	|	geometry.colors = colors;
	|
	|	material = new THREE.ParticleSystemMaterial( { size: 65, map: sprite, vertexColors: true, transparent: true } );
	|	material.color.setHSL( 1.0, 0.2, 0.7 );
	|
	|	particles = new THREE.ParticleSystem( geometry, material );
	|	particles.sortParticles = true;
	|
	|	//Add Particles once the sprite tags are replaced with html
	|	scene.add( particles );
	|
	| The next set of lines will create the plane used by the menu.
	*/

    plane = new THREE.Mesh( new THREE.PlaneGeometry( 2000, 2000, 8, 8 ), new THREE.MeshBasicMaterial( { color: 0x000000, opacity: 0.25, transparent: true, wireframe: true } ) );
    plane.visible = false;
	
    scene.add( plane );

    projector = new THREE.Projector();

	/*
	|--------------------------------------------------------------------------
	| Render Logic
	|--------------------------------------------------------------------------
	|
	| Creates a Render for our scene.
	|
	*/
	
    renderer = new THREE.WebGLRenderer( { antialias: true } );
    renderer.setClearColor( 0x000000 );
    renderer.setSize( window.innerWidth, window.innerHeight );
    renderer.sortObjects = false;

    renderer.shadowMapEnabled = true;
    renderer.shadowMapType = THREE.PCFShadowMap;
    
	/*
	|--------------------------------------------------------------------------
	|--------------------------------------------------------------------------
	|
	|
	*/
	
    var menu_div = document.createElement("div");
    menu_div.className = "menu_wrapper menu_wrapper_flex";

    // sets up the initial sidebar search box and sections to contain the dynamically loaded categories
    menu_div.innerHTML = '<div id="active_area" class="wrapper static"><form></form></div><div class="flex"><div id="search_area" class="wrapper flex-child"><form><input type="text" class="search_field"><span id="search_result_area"></span></form></div>';
        
    document.body.appendChild( menu_div );

    document.body.appendChild( renderer.domElement ); // adds the renderer WebGL canvas to the document

    // binds event listener functions for mouse interaction
    renderer.domElement.addEventListener( 'mousemove', onDocumentMouseMove, false );
    renderer.domElement.addEventListener( 'mousedown', onDocumentMouseDown, false );
    renderer.domElement.addEventListener( 'mouseup', onDocumentMouseUp, false );

    // imports controls that allow rotation and zooming in the space with the mouse
    controls = new THREE.TrackballControls( camera, renderer.domElement );
    controls.rotateSpeed = 0.5;
    controls.zoomSpeed = 1.0;
    controls.panSpeed = 0.5;
    controls.noZoom = false;
    controls.noPan = true;
    controls.staticMoving = true;
    controls.dynamicDampingFactor = 0.3;

    // binds event to resize the renderer when the document changes size
    window.addEventListener( 'resize', onWindowResize, false );
    
    // instantiates a Backbone model for documents (not used, as of right now)
    DocumentModel = Backbone.Model.extend({});
    DocumentModelList = Backbone.Collection.extend({url: "./api/v1/documents", model: DocumentModel});

    Documents = new DocumentModelList();

    Documents.on("reset", function(list) {
	    console.log("Documents: " + list.length);
	    $('#doc_select').html('');
	    list.each(function(doc) {
		    $('#doc_select').append('<option value="d' + doc.get("Document ID") + '">' + doc.get("File Name") + '</option>');
		});
	    $('#doc_select').checkboxField({prefix: 'doc'});
	});

    // the following line enables document fetch
    //Documents.fetch({reset: true});

    // initializes the default search term (this will be replaced eventually by a weighted-by-importance set of categories)
    window.search_term = "enron";

    // defines a Backbone model for category data
    CategoryModel = Backbone.Model.extend({});
    // defines a Backbone collection for fetching a list of categories from the API, constrained by the search term
    CategoryModelList = Backbone.Collection.extend({url: function() { return "./api/v1/categories/search/" + window.search_term; }, model: CategoryModel});

    // instantiates the Backbone category collection
    Categories = new CategoryModelList();

    // binds the rendering logic for the list of categories in the sidebar when a new list is loaded (i.e., via search)
    Categories.on("reset", function(list) {
            console.log("Categories: " + list.length);
            $('#search_result_area').html('');
            list.each(function(cat) {
                    $('#search_result_area').append('<div><input type="checkbox" id="c' + cat.get("Category ID") + '" data-color="#' + (Math.pow(hashCode(cat.get("Category String")), 12) % 0xffffff).toString(16) + '"/><label for="c' + cat.get("Category ID") + '">' + cat.get("Category String") + '</label></div>');
                });
            // binds the logic to color the checkboxes properly when selected/deselected
            $(":checkbox").change(function() {
                    if(this.checked) {
                        // calculates a darker color for the checkbox borders
                        var color = $(this).data('color');
                        var num = parseInt(color.substr(1),16);
                        var R = (num >> 16);
                        var G = (num >> 8 & 0x00FF);
                        var B = (num & 0x0000FF);
                        var new_R = (R + 0x55)/2;
                        var new_G = (G + 0x55)/2;
                        var new_B = (B + 0x55)/2;
                        var darker_color = "#" + Math.ceil(new_R).toString(16) + Math.ceil(new_G).toString(16) + Math.ceil(new_B).toString(16);

                        $(this).css('border-color', darker_color);
                        $(this).css('background-color', color);
                        $(this).parent().addClass("enabled");

                    } else {
                        $(this).css('background-color', '');
                        $(this).css('border-color', '');
                        $(this).parent().removeClass("enabled");
                    }
                    $(this).blur(); // bugfix for scroll-to-top problem; if an item is still selected, it won't scroll to the top of the list to display the legend
                    window.changeSelected(); // pulls data from the API and re-renders the graph based on the newly selected categories
                });
        });

    Categories.fetch({reset: true}); // initial fetch of category list

}

/*
|--------------------------------------------------------------------------
| Window Resize
|--------------------------------------------------------------------------
|
| Handles the logic for when the Projection matrix needs to be updated due
| to window transforms.
|
*/

function onWindowResize() {

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight );

}

/*
|--------------------------------------------------------------------------
| Mouse Movement 
|--------------------------------------------------------------------------
|
| Updates the event handler for the when the mouse is moved over the scene.
|
*/

function onDocumentMouseMove( event ) {

    event.preventDefault();

    mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
    mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;

    var vector = new THREE.Vector3( mouse.x, mouse.y, 0.5 );
    projector.unprojectVector( vector, camera );

    var raycaster = new THREE.Raycaster( camera.position, vector.sub( camera.position ).normalize() );
}

/*
|--------------------------------------------------------------------------
| Mouse down camera manipulation
|--------------------------------------------------------------------------
|
| This block of code handles the meat of the camera manipulation. When
| A sceneObject is clicked, the camera will rotate to it's position.
|
*/

function onDocumentMouseDown( event ) {


    console.log(event);

    event.preventDefault();

    mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
    mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;

    var vector = new THREE.Vector3( mouse.x,mouse.y, 1 );
    projector.unprojectVector( vector, camera );
    var raycaster = new THREE.Raycaster( camera.position, vector.sub( camera.position ).normalize() );

    var intersects = raycaster.intersectObjects( sceneObjects );

	/*
	|--------------------------------------------------------------------------
	| Intersection logic
	|--------------------------------------------------------------------------
	|
	| This if statement will check whether or not the user has clicked on a viable
	| sceneObject.
	|
	*/
	
    if ( intersects.length > 0 ) {

		/*
		|--------------------------------------------------------------------------
		| SceneObjects X,Y,Z Coordinates
		|--------------------------------------------------------------------------
		|
		| Grabs the coordinates of the specified sceneObject.
		|
		*/
		
        var xIntersect = intersects[ 0 ].object.position.x;
        var yIntersect = intersects[ 0 ].object.position.y;
        var zIntersect = intersects[ 0 ].object.position.z;

		/*
		|--------------------------------------------------------------------------
		| NewCP Vector 
		|--------------------------------------------------------------------------
		|
		| Creates a new vector based upon the coordinates of the scene object
		| which is then multiplied by a scaler. 
		|
		*/
		
		var newCP = new THREE.Vector3(xIntersect,yIntersect,zIntersect).multiplyScalar(2);

		/*
		|--------------------------------------------------------------------------
		| Parameters check
		|--------------------------------------------------------------------------
		|
		| We must account for a coordinate if it is less 0 and greater than -1
		| and if it is greater than 0 and less than 1. These coordinates for 
		| the z axis will cause the camera to zoom in to close, so a simple 
		| correction needs to be made by multiplying it by 100.
		|
		*/
		
        if((zIntersect >= 0 && zIntersect <= 1) || (zIntersect <= 0 && zIntersect >= -1))
        {
            zIntersect = intersects[ 0 ].object.position.z * 100;
        }
	
		/*
		|--------------------------------------------------------------------------
		| Tween Camera
		|--------------------------------------------------------------------------
		|
		| The next block of code will transition the camera to the sceneObjects 
		| position. This will add in the new vector newCP to give the camera some
		| space from the object in question.
		| 
		|
		*/
        new TWEEN.Tween( camera.position).to( {
            x: xIntersect + newCP.x,
			y: yIntersect + newCP.y,
            z: zIntersect + newCP.z
        }, 600)
            .easing( TWEEN.Easing.Sinusoidal.Out).start();

        new TWEEN.Tween( light.position).to( {
            x: xIntersect + newCP.x,
            y: yIntersect + newCP.y,
            z: zIntersect + newCP.z
        }, 600)
            .easing( TWEEN.Easing.Sinusoidal.Out).start();

    }
	console.log(event);
	
		/*
		|--------------------------------------------------------------------------
		| Outline/Highlights
		|--------------------------------------------------------------------------
		|
		| The next block of code will make a intersected object appear highlighted
		| by setting the mesh that each sphere object has present to visible.
		| Allowing for a nice halo effect around the clicked object.
		| 
		|
		*/

        if ( intersects.length > 0 ) {

            if ( intersects[ 0 ].object != OUTLINE )
            {
                if ( OUTLINE ) OUTLINE.highLight.visible = false;
                OUTLINE = intersects[ 0 ].object;
                OUTLINE.highLight.visible = true;
            }
        }
        else
        {
            if (OUTLINE) OUTLINE.highLight.visible = false;
            OUTLINE  = null;
        }

}

/*
|--------------------------------------------------------------------------
| Mouse Movement Up
|--------------------------------------------------------------------------
|
| Updates the event handler for the when the mouse is moved over the scene.
|
*/

function onDocumentMouseUp( event ) {

    event.preventDefault();

    controls.enabled = true;

    if ( INTERSECTED ) {

        plane.position.copy( INTERSECTED.position );

        SELECTED = null;

    }
    document.body.style.cursor = 'auto';

}

/*
|--------------------------------------------------------------------------
| Animate
|--------------------------------------------------------------------------
|
| Updates the animation handler for the scene. When the user hovers over
| an objects its pre-set tags will be displayed as the animation loop updates. 
|
*/

function animate() {

    requestAnimationFrame( animate );

    render();

    /*
    | Handles hiding or showing labels on mouseover/out
    */
    var vector = new THREE.Vector3( mouse.x, mouse.y, 1 );
    projector.unprojectVector( vector, camera );

    var ray = new THREE.Raycaster( camera.position, vector.sub( camera.position ).normalize() );

    var intersects = ray.intersectObjects( sceneObjects );
    
    if ( intersects.length > 0 ) {
        
        if ( intersects[ 0 ].object != LABEL_INTERSECTED ) 
            {
                if ( LABEL_INTERSECTED ) LABEL_INTERSECTED.labelSprite.visible = false;
                LABEL_INTERSECTED = intersects[ 0 ].object;
                LABEL_INTERSECTED.labelSprite.visible = true;
            }
        
    } else {

        if (LABEL_INTERSECTED) LABEL_INTERSECTED.labelSprite.visible = false;
        LABEL_INTERSECTED = null;
    }
}
/*
|--------------------------------------------------------------------------
| Render
|--------------------------------------------------------------------------
|
| And lastly our render object that will handle our control updates.
|
*/
function render() {
	
	//Count to shift colors of back sprites.
	//var time = Date.now() * 0.000005;
	
    TWEEN.update();
    controls.update();
	
	/*Colour rendering
    for ( i = 0; i < scene.children.length; i ++ ) 
	{

        var object = scene.children[ i ];

        if ( object instanceof THREE.ParticleSystem ) 
		{
            object.rotation.y = time * ( i < 4 ? i + 1 : - ( i + 1 ) );
        }
    }
	*/

    renderer.render( scene, camera );

}