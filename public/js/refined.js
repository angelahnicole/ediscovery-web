// Author: Scott Halstvedt

window.debug = true; // enables logging for some parts

// defines a Backbone model and collection for the graph objects pulled from the API (distinct from categories)
CategoryGraphModel = Backbone.Model.extend({});
CategoryGraphList = Backbone.Collection.extend({url: "./api/v1/categories/graph", model: CategoryGraphModel});

// instantiates the graph collection
Graph = new CategoryGraphList();

// this function, called when a checkbox is selected or deselected, fetches a new copy of the graph directly, passing POST-data with the currently selected categories (and, for now, an empty set of pseudo-categories).
window.refreshGraph = function() { Graph.fetch({reset:true, data: {selectedCategories: window.selectedString, selectedGenerated: '[]'}, type: 'POST'}); };

function hashCode(str) { // similar to java String#hashCode; generates a hash based on a string for color selection
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    return hash;
} 

function intToARGB(i){ // converts an integer to a Red/Green/Blue hex string that can be used directly in CSS
    return ((i>>24)&0xFF).toString(16) + 
        ((i>>16)&0xFF).toString(16) + 
        ((i>>8)&0xFF).toString(16) + 
        (i&0xFF).toString(16);
}


function buildGraph(realList, generatedList) { // based on a graph collection fetched from the API, creates a Springy graph representation with nodes and edges, and binds the original collection's data (label and numEItems)
    graph = new Springy.Graph();
    realList.each(function(category, i) { // these are the "real" category nodes
	    var node = graph.newNode({label: category.get("Category String"), numEItems: category.get("Num EItems"), generated: false});
	    category.set("graphNode", node);
	});
    generatedList.each(function(generated, i) { // these are the "intersection" compound-category nodes, which are rendered in a different color
            var node = graph.newNode({label: generated.get("Category String"), numEItems: generated.get("Num EItems"), generated: true});
            generated.set("graphNode", node);
            var ids = generated.get("Categories");
            for (var i = 0; i < ids.length; i++) { // for the generated nodes, edges in the graph are created between all categories included
                var edge = graph.newEdge(node, realList.find(function(m) { return m.get("Category ID") == ids[i] }).value().get("graphNode"), {'color':'#000000'});
            }
	});
    return graph;
}

Graph.on("reset", function() { // called when a new graph is loaded
        var objsToRemove = _.rest(scene.children, 1); // remove all objects from the scene to reset
        _.each(objsToRemove, function( object ) {
                scene.remove(object);
            });
        doLights(); // re-insert removed lights
        // filter non-generated from generated (compound) nodes
        var catList = Graph.chain().filter(function(m) { return m.get("Is Generated") == 0 });
        var genList = Graph.chain().filter(function(m) { return m.get("Is Generated") == 1 });
        window.myGraph = buildGraph(catList, genList); // convert the collections to a Springy graph
        layout = new Springy.Layout.ForceDirected(myGraph, 800.0, 600.0, 0.5); // parametrizes force-directed layout on the Springy graph
        theRenderer = new ThreeGraphRenderer(layout, function() { if (window.debug) {console.log("stopped layout")} }, function() { if(window.debug) {console.log("started layout")} }); // instantiate a new ThreeGraphRenderer
        theRenderer.start(); // begin the layout and rendering loops
    });
    
function makeTextSprite( message, parameters ) // creates mouseover label sprites
{
    if ( parameters === undefined ) parameters = {};

    var fontface = parameters.hasOwnProperty("fontface") ?
        parameters["fontface"] : "Arial";

    var fontsize = parameters.hasOwnProperty("fontsize") ?
        parameters["fontsize"] : 25;

    var borderThickness = parameters.hasOwnProperty("borderThickness") ?
        parameters["borderThickness"] : 4;

    var borderColor = parameters.hasOwnProperty("borderColor") ?
        parameters["borderColor"] : { r:0, g:0, b:0, a:1.0 };

    var backgroundColor = parameters.hasOwnProperty("backgroundColor") ?
        parameters["backgroundColor"] : { r:255, g:255, b:255, a:1.0 };

    console.log(THREE.SpriteAlignment);

	//Sets the sprite to the top of each node
    var spriteAlignment = THREE.SpriteAlignment.topLeft;

    var canvas = document.createElement('canvas');
    var context = canvas.getContext('2d');
    context.font = "Bold " + fontsize + "px " + fontface;

    // get size data (height depends only on font size)
    var metrics = context.measureText( message );
    var textWidth = metrics.width;
    var maxWidth = 285;
    // background color
    context.fillStyle   = "rgba(" + backgroundColor.r + "," + backgroundColor.g + ","
        + backgroundColor.b + "," + backgroundColor.a + ")";
    // border color
    context.strokeStyle = "rgba(" + borderColor.r + "," + borderColor.g + ","
        + borderColor.b + "," + borderColor.a + ")";

    context.lineWidth = borderThickness;
	
	//Corrections to the rectangle
    roundRect(context, borderThickness/2, borderThickness/2, canvas.width - 5, lineHeight(context, message, 30, 300, 25) + 10, 15);
    // 1.4 is extra height factor for text below baseline: g,j,p,q.

    // text color
    context.fillStyle = "rgba(0, 0, 0, 1.0)";
	
	//Wrap the text
    wrapText(context, message, (canvas.width - maxWidth)/ 2 , 30, 300, 25);
    // canvas contents will be used for a texture
    var texture = new THREE.Texture(canvas)
    texture.needsUpdate = true;

    var spriteMaterial = new THREE.SpriteMaterial(
        { map: texture, useScreenCoordinates: false, alignment: spriteAlignment } );
    var sprite = new THREE.Sprite( spriteMaterial );

    sprite.visible = false;
	
	//Size corrections
    sprite.scale.set(250,150,1.0);
    return sprite;
}

function roundRect(ctx, x, y, w, h, r)
{
    ctx.beginPath();
    ctx.moveTo(x+r, y);
    ctx.lineTo(x+w-r, y);
    ctx.quadraticCurveTo(x+w, y, x+w, y+r);
    ctx.lineTo(x+w, y+h-r);
    ctx.quadraticCurveTo(x+w, y+h, x+w-r, y+h);
    ctx.lineTo(x+r, y+h);
    ctx.quadraticCurveTo(x, y+h, x, y+h-r);
    ctx.lineTo(x, y+r);
    ctx.quadraticCurveTo(x, y, x+r, y);
    ctx.closePath();
    ctx.fill();
    ctx.stroke();
}
     //Wraps a string of text
    function wrapText(context, text, x, y, maxWidth, lineHeight) {
        var words = text.split(' ');
        var line = '';

        for(var n = 0; n < words.length; n++) {
            var testLine = line + words[n] + ' ';
            var metrics = context.measureText(testLine);
            var testWidth = metrics.width;
            if (testWidth > maxWidth && n > 0) {
                context.fillText(line, x, y);
                line = words[n] + ' ';
                y += lineHeight;
            }
            else {
                line = testLine;
            }
        }
        context.fillText(line, x, y);
    }
    
	//Allows for each line to increment properly
    function lineHeight(context, text,y, maxWidth, lineHeight) {
        var words = text.split(' ');
        var line = '';

        for(var n = 0; n < words.length; n++) {
            var testLine = line + words[n] + ' ';
            var metrics = context.measureText(testLine);
            var testWidth = metrics.width;
            if (testWidth > maxWidth && n > 0) {
                line = words[n] + ' ';
                y += lineHeight;
            }
            else {
                line = testLine;
            }
        }
        return y;
    }

	 function hex2r( colour ) {
        var r;
        console.log(colour);

        r = colour.charAt(0) + '' + colour.charAt(1);
        console.log(r);
        r = parseInt( r,16 );
        return r ;
    }
    function hex2g( colour ) {
        var g;
        console.log(colour);
        g = colour.charAt(2) + '' + colour.charAt(3);
        console.log(g);
        g = parseInt( g,16 );

        return g;
    }
    function hex2b( colour ) {
        var b;
        console.log(colour);
        b = colour.charAt(4) + '' + colour.charAt(5);
        console.log(b);
        b = parseInt( b,16);
        return b;
    }

var sceneObjects = [];
var arrayG = [];
// ThreeGraphRenderer uses a Springy graph representation to generate spheres in Three.js, and keeps their positions up-to-date as the layout runs and improves
var ThreeGraphRenderer = function(layout, onRenderStart, onRenderStop) {
    this.layout = layout;
    this.onRenderStart = onRenderStart;
    this.onRenderStop = onRenderStop;

    this.layout.graph.addGraphListener(this);

    this.layout.eachNode(function(node, point) {
        var sphereSize = Math.pow(node.data.numEItems, 0.5) * 5;
        var geometry = new THREE.SphereGeometry(sphereSize,32,32);
		
		//Color Properties
		var nodeColor = new THREE.Color(Math.pow(hashCode(node.data.label), 12) % 0xffffff);
        var color = node.data.generated ? 0xDCDC00 : nodeColor;
		var nColor = nodeColor.getHexString();
		var spriteR = hex2r(nColor);
		var spriteG = hex2g(nColor);
		var spriteB = hex2b(nColor);
		
        var material = new THREE.MeshLambertMaterial( { color: color } );
	    var object = new THREE.Mesh( geometry, material );
	    object.material.ambient = object.material.color;
	    object.position.x = point.p.x*150;
	    object.position.y = point.p.y*150;
	    object.position.z = node.data.generated ? 300 : 0;
	    node.data.threeObject = object;
        sceneObjects.push(object);
        scene.add(object);
        
        var spriteColor = node.data.generated ? {r:255, g:204, b:102, a:1.0} : {r:spriteR - 20, g:spriteG - 20, b:spriteB - 20, a:1.0};
        var spriteBgColor = node.data.generated ? {r:255, g:255, b:51, a:1.0} : {r:spriteR + 40, g:spriteG + 40, b:spriteB + 40, a:1.0};
        var spritey = makeTextSprite(node.data.label, { fontsize: 25, borderColor: spriteColor, backgroundColor: spriteBgColor } );
        spritey.position.set(object.position.x+(Math.pow(node.data.numEItems, 0.5) * 5)+25, object.position.y+(Math.pow(node.data.numEItems, 0.5) * 5)+25, object.position.z+(Math.pow(node.data.numEItems, 0.5) * 5)+25);
        object.labelSprite = spritey;
        scene.add(spritey);

		var outlineMaterial1 = new THREE.MeshBasicMaterial( { color: 0xffffff, transparent: true,opacity:0.5, blending: THREE.AdditiveBlending} );
		var outlineMesh1 = new THREE.Mesh( geometry, outlineMaterial1 );
		outlineMesh1.scale.multiplyScalar(1.05);
		outlineMesh1.position = object.position;
		outlineMesh1.visible = false;
	    object.highLight = outlineMesh1;
		scene.add( outlineMesh1);
		arrayG.push(outlineMesh1);
	});
    this.layout.eachEdge(function(node, spring) { // renders each edge as a blue line
            var myGeo = new THREE.Geometry();
            myGeo.vertices[0] = new THREE.Vector3(spring.point1.p.x*150, spring.point1.p.y*150, 300);
            myGeo.vertices[1] = new THREE.Vector3(spring.point2.p.x*150, spring.point2.p.y*150, 0);
            var myMat = new THREE.LineBasicMaterial({color: 0x6699FF, linewidth: 5});
            var myEdge = new THREE.Line(myGeo, myMat);
            myEdge.type = THREE.Lines;
            node.data.threeObject = myEdge;
            scene.add(myEdge);
        });

};

ThreeGraphRenderer.prototype.graphChanged = function(e) { // if the graph is changed in structure, re-start the layout
    console.log("Renderer detected graph change");
    this.layout.start();
};

// ThreeGraphRenderer's rendering loop starts the force-directed layout
ThreeGraphRenderer.prototype.start = function(done) {
    var t = this;
    t.layout.start(function render() {
            // here goes position updates
            t.layout.eachNode(function (node, point) {
                    node.data.threeObject.position.x = point.p.x*150;
                    node.data.threeObject.position.y = point.p.y*150;
                    node.data.threeObject.labelSprite.position.set(node.data.threeObject.position.x+(Math.pow(node.data.numEItems, 0.5) * 5)+25, node.data.threeObject.position.y+(Math.pow(node.data.numEItems, 0.5) * 5)+25, node.data.threeObject.position.z+(Math.pow(node.data.numEItems, 0.5) * 5)+25);
                });
            t.layout.eachEdge(function (edge, spring) {
                    edge.data.threeObject.geometry.vertices[0].x = spring.point1.p.x*150;
                    edge.data.threeObject.geometry.vertices[0].y = spring.point1.p.y*150;
                    edge.data.threeObject.geometry.vertices[1].x = spring.point2.p.x*150;
                    edge.data.threeObject.geometry.vertices[1].y = spring.point2.p.y*150;
                    edge.data.threeObject.geometry.verticesNeedUpdate = true;
                });
        }, this.onRenderStart, this.onRenderStop);
};

ThreeGraphRenderer.prototype.stop = function(done) {
    this.layout.stop();
};
