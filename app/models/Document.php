<?php namespace App\Models;

use Eloquent;
/* ===================================================================================================
// Document.php
// ---------------------------------------------------------------------------------------------------
// eDiscovery Web Application
//
// [Advanced Programming Team](https://bitbucket.org/umadvprog)
//
// @author Angela Gross
//
// Model for the Document table. Contains basic relationship and table information, such as
// getting a Document's associated FileTables and EItems.
// ===================================================================================================*/

class Document extends Eloquent
{
    /////////////////////////////////////////////////////////////////////////////////////////

    //### TABLE used by model
    // @var `string`
    protected $table = 'EitemData.Document';

    //### PRIMARY KEY override
    // @var `string`
    protected $primaryKey = 'Document ID';

    public $hidden = array('File ID', 'E-item ID');

    /////////////////////////////////////////////////////////////////////////////////////////

    //### RELATIONS

    // Retrieves a Document's associated FileTables
    // @return mixed
    public function FileTable() { return $this->belongsTo('App\Models\FileTable', 'File ID'); }

    // Retrieves a Document's associated EItems
    // @return mixed
    public function EItem() { return $this->belongsTo('App\Models\EItem', 'E-item ID'); }

    /////////////////////////////////////////////////////////////////////////////////////////
}