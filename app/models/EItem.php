<?php namespace App\Models;

use Eloquent, DB;
/* ===================================================================================================
// EItem.php
// ---------------------------------------------------------------------------------------------------
// eDiscovery Web Application
//
// [Advanced Programming Team](https://bitbucket.org/umadvprog)
//
// @author Angela Gross
//
// Model for the EItem table. Contains basic relationship and table information, such as
// getting an EItem's associated Document, Email, and CategoryEItem join tables.
// ===================================================================================================*/

class EItem extends Eloquent
{
    /////////////////////////////////////////////////////////////////////////////////////////

    //### TABLE used by model
    // @var `string`
    protected $table = 'EitemData.E-item';

    //### PRIMARY KEY override
    // @var `string`
    protected $primaryKey = 'E-item ID';

    /////////////////////////////////////////////////////////////////////////////////////////

    //### RELATIONS

    // Retrieves a EItem's associated Document
    // @return mixed
    public function Document() { return $this->hasOne('App\Models\Document', 'E-item ID'); }

    // Retrieves a EItem's associated Email
    // @return mixed
    public function Email() { return $this->hasOne('App\Models\EmailItem', 'E-item ID'); }

    // Retrieves a EItem's associated CategoryEItem join tables
    // @return mixed
    public function Categories() { return DB::table((new CategoryEItem())->getTable())->where('E-item ID', '=', $this->{'E-item ID'})->get(); }

    /////////////////////////////////////////////////////////////////////////////////////////
}