<?php namespace App\Models;

use Eloquent;

/* ===================================================================================================
// CategoryEItem.php
// ---------------------------------------------------------------------------------------------------
// eDiscovery Web Application
//
// [Advanced Programming Team](https://bitbucket.org/umadvprog)
//
// @author Angela Gross
//
// Model for the CategoryEItem join table. Contains basic relationship and table information, such as
// getting a Category's associated EItems and getting an EItem's associated Categories.
// ===================================================================================================*/

class CategoryEItem extends Eloquent
{
    /////////////////////////////////////////////////////////////////////////////////////////

    //### TABLE used by model
    // @var `string`
    protected $table = 'CategoryData.Category-E-item';

    //### PRIMARY KEY override
    // @var `string`
    protected $primaryKey = 'Category Item ID';

    /////////////////////////////////////////////////////////////////////////////////////////

    //### RELATIONS

    // Retrieves a Category's associated EItems
    // @return `mixed`
    public function EItem() { return $this->belongsTo('App\Models\EItem', 'E-item ID'); }

    // Retrieves an EItem's associated Categories.
    // @return `mixed`
    public function Category() { return $this->belongsTo('App\Models\Category', 'Category ID'); }

    /////////////////////////////////////////////////////////////////////////////////////////
}