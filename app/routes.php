<?php

/* ===================================================================================================
// routes.php
// ---------------------------------------------------------------------------------------------------
// eDiscovery Web Application
//
// [Advanced Programming Team](https://bitbucket.org/umadvprog)
//
// @authors Angela Gross, Scott Halstvedt
//
// These routes help redirect URLs to their proper places. This is used for the main page, the API,
// and for the documentation pages.
// ===================================================================================================*/

//////////////////////////////////////////////////////////////////////////////////////////////////////

//### MAIN PAGE of eDiscovery
// Displays the "meat" of the program, or the part that the end-user will interact with

Route::get('/', 'App\Controllers\MainController@showHome');

//////////////////////////////////////////////////////////////////////////////////////////////////////

//### API for eDiscovery
// Used to talk between Backbone.js and the DB (and Laravel provides a layer of abstraction)
// -----------------------------------------------------------------------------------------------

//#### Version 1 of the API
Route::group(array('prefix' => '/api/v1/'), function()
{

/* ##### _DOCUMENT ROUTES_ that help retrieve documents and their relations
// -----------------------------------------------------------------------------------------------*/
    Route::group(array('prefix' => 'documents'), function()
    {
        // Plain text search of file names (and paths)
        Route::any('/search/{term}', array('as' => 'api.documents.search', 'uses' => 'App\Controllers\API\V1\DocumentAPI@search'));

        // Grab all (or a specific) document
        Route::any('{documentID?}', array('as' => 'api.documents', 'uses' => 'App\Controllers\API\V1\DocumentAPI@documents'));
    });

/* ##### _EITEM ROUTES_ that help retrieve eItems and their relations
// -----------------------------------------------------------------------------------------------*/
    Route::group(array('prefix' => 'eitems'), function()
    {
        // Grab all (or a specific) eItem
        Route::any('{eItemID?}', array('as' => 'api.eitems', 'uses' => 'App\Controllers\API\V1\EItemAPI@eItems'));
    });

/* ##### _CATEGORY ROUTES_ that help retrieve categories and their relations (including dynamically created intersections)
// -----------------------------------------------------------------------------------------------*/
    Route::group(array('prefix' => 'categories'), function()
    {
        // Plain text search of categories
        Route::any('/search/{term}', array('as' => 'api.categories.search', 'uses' => 'App\Controllers\API\V1\CategoryAPI@search'));

        // Get the number of all categories
        Route::any('num', array('as' => 'api.categories.num', 'uses' => 'App\Controllers\API\V1\CategoryAPI@getNumCategories'));

        // Generate nodes based on eItems
        Route::any('graph', array('as' => 'api.categories.graph', 'uses' => 'App\Controllers\API\V1\CategoryAPI@graphCategories'));

        // Grab all (or a specific) category
        Route::any('{categoryID?}', array('as' => 'api.categories', 'uses' => 'App\Controllers\API\V1\CategoryAPI@categories'));
    });
});

//////////////////////////////////////////////////////////////////////////////////////////////////////

//### Documentation for eDiscovery
/* Used to view documentation with [Phrocco](http://phrocco.info/), a documentation generator
// for PHP.
// ----------------------------------------------------------------------------------------------- */

Route::group(array('prefix' => 'docs'), function()
{
    /* ##### _HOME DOC ROUTE_ that takes the user to our documentation index
// -----------------------------------------------------------------------------------------------*/
   Route::any('/', function(){ return View::make('docs.index');}) ;

    /* ##### _DOC ROUTE_ that documents this very file, routes.php
// -----------------------------------------------------------------------------------------------*/
    Route::any('/routes', function(){ return View::make('docs.routes');}) ;

    /* ##### _MODEL DOC ROUTES_ that document all database models in the application
// -----------------------------------------------------------------------------------------------*/
    Route::group(array('prefix' => 'models'), function()
    {
        Route::any('', function(){ return View::make('docs.models.Category');}) ;
        Route::any('Category', function(){ return View::make('docs.models.Category');}) ;
        Route::any('CategoryEItem', function(){ return View::make('docs.models.CategoryEItem');}) ;
        Route::any('Document', function(){ return View::make('docs.models.Document');}) ;
        Route::any('EItem', function(){ return View::make('docs.models.EItem');}) ;
        Route::any('EmailItem', function(){ return View::make('docs.models.EmailItem');}) ;
        Route::any('FileTable', function(){ return View::make('docs.models.FileTable');}) ;

    });

    /* ##### _API DOC ROUTES_ that document all API Controllers in the application
// -----------------------------------------------------------------------------------------------*/
    Route::group(array('prefix' => 'API'), function()
    {
        Route::any('', function(){ return View::make('docs.API.CategoryAPI');}) ;
        Route::any('CategoryAPI', function(){ return View::make('docs.API.CategoryAPI');}) ;
        Route::any('DocumentAPI', function(){ return View::make('docs.API.DocumentAPI');}) ;
        Route::any('EItemAPI', function(){ return View::make('docs.API.EItemAPI');}) ;


    });

});

//////////////////////////////////////////////////////////////////////////////////////////////////////