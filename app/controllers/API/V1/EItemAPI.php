<?php namespace App\Controllers\API\V1;

use App\Models\EItem;
use App\Models\Category;
use BaseController, Response;

/* ===================================================================================================
// EItemAPI.php
// ---------------------------------------------------------------------------------------------------
// eDiscovery Web Application
//
// [Advanced Programming Team](https://bitbucket.org/umadvprog)
//
// @authors Angela Gross, Scott Halstvedt
//
// Allows user to view all EItems or just some information about a particular EItem.
// ===================================================================================================*/

class EItemAPI extends BaseController 
{
    //////////////////////////////////////////////////////////////////////////////////////////////////


/* ======================================================================================
//### eItems()
// --------------------------------------------------------------------------------------
// If a single EItem is selected, then it grabs all associated categories along with the
// file. If all EItems are selected (no ID is given) then just grabs file information.
//
// @param `int $eItemID`
//
// @return `JSON`
// ======================================================================================*/
		public function eItems($eItemID = null)
		{
            // If no eItem ID is specified, then grab all eItems
			if (is_null($eItemID)) 
			{
                $eItems = array();

                // Format each EItem with documents and ID
                foreach(EItem::all() as $eItem)
                {
                    $document = $eItem->Document;
                    $file = null;

                    // Does it have a document? If not, then try an email
                    if(is_null($document))
                    {
                        $email = $eItem->Email;

                        // Does it have an email? If so, grab the file
                        if(!is_null($email))
                        {
                            $file = $email->FileTable;
                        }
                    }
                    // If it does have a document, grab the file
                    else
                    {
                        $file = $document->FileTable;
                    }

                    $eItems[] = array
                    (
                        'E-item ID' => $eItem->{'E-item ID'},
                        'File' => $file->toArray()
                    );
                }

				return $eItems;
			} 
			else 
			{
				$eItem = EItem::find($eItemID);

                // If eItem not found, tell user.
				if (is_null($eItem))
				{
					return Response::json('E-Item not found', 404);
				}
                // Else, format eItem and return it
				else 
				{
                    $document = $eItem->Document;
                    $file = null;
                    $categories = $eItem->Categories();

                    // Does it have a document? If not, then try an email
                    if(is_null($document))
                    {
                        $email = $eItem->Email;

                        // Does it have an email? If so, grab the file
                        if(!is_null($email))
                        {
                            $file = $email->FileTable;
                        }
                    }
                    // If it does have a document, grab the file
                    else
                    {
                        $file = $document->FileTable;
                    }

                    $allCategories = array();

                    // Format categories
                    foreach($categories as $category)
                    {
                        $category = Category::find($category->{'Category ID'});
                        $allCategories[] = array
                        (
                            'Category ID' => $category->{'Category ID'},
                            'Category String' => $category->{'Category String'}
                        );
                    }

                    $formatEItem = array
                    (
                        'E-item ID' => $eItem->{'E-item ID'},
                        'File' => $file->toArray(),
                        'Categories' => $allCategories
                    );

					return $formatEItem;
				}
			}
		}
	  
    //////////////////////////////////////////////////////////////////////////////////////////////////
}