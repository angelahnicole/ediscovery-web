<?php namespace App\Controllers\API\V1;

use App\Models\FileTable;
use BaseController, Response, DB;

/* ===================================================================================================
// DocumentAPI.php
// ---------------------------------------------------------------------------------------------------
// eDiscovery Web Application
//
// [Advanced Programming Team](https://bitbucket.org/umadvprog)
//
// @authors Angela Gross, Scott Halstvedt
//
// This API combines Documents, Email Items, and FileTable tables into one interface. The phrase
// Documents" refers to all three of those tables loosely, but mostly refers to the FileTable
// rows.
// ===================================================================================================*/

class DocumentAPI extends BaseController
{
    //////////////////////////////////////////////////////////////////////////////////////////////////

/* ======================================================================================
//### documents()
// --------------------------------------------------------------------------------------
//
// Grabs all files. This makes the generalization of using documents and email items and
// grabbing them all from the file-table. If an ID is provided, then it finds the related
// FILETABLE entry.
*/
// @param `int $documentID`
//
// @return `JSON`
// ======================================================================================*/
    public function documents($documentID = null)
    {
        // If there is no id, then return all documents in JSON format
        if (is_null($documentID))
        {
            $allDocuments = array();

            foreach (FileTable::all() as $file)
            {
                // Reset temp arrays
                $tempEmails = array();
                $tempDocuments = array();

                // Get associated emails documents
                $emails = $file->Emails();
                $documents = $file->Documents();

                // Format each email (if there are any)
                foreach($emails as $email)
                {
                    $tempEmails[] = array
                    (
                        'Email ID' => $email->{'Email ID'},
                        'E-item ID' => $email->{'E-item ID'},
                        'To' => $email->{'To'},
                        'Sender' => $email->{'Sender'},
                        'Date Sent' => $email->{'Date Sent'},
                        'Subject' => $email->{'Subject'}
                    );
                }

                // Format each document (if there are any)
                foreach($documents as $document)
                {
                    $tempDocuments[] = array
                    (
                        'Document ID' => $document->{'Document ID'},
                        'E-item ID' => $document->{'E-item ID'}
                    );
                }

                // Add specific file information
                $allDocuments[] = array
                (
                    'File Name' => $file->{'File Name'},
                    'File Path' => $file->{'File Path'},
                    'File Type' => $file->{'File Type'},
                    'Email Items' => $tempEmails,
                    'Documents' => $tempDocuments
                );
            }

            return Response::make(json_encode($allDocuments), 200, array('Content-Type' => 'application/json'));
        }
        // Try to find document with given ID
        else
        {
            $file = FileTable::find($documentID);

            // If it is null, then tell user.
            if (is_null($file))
            {
                return Response::json('Document not found', 404);
            }
            // Else, return it to user.
            else
            {
                return $file;
            }
        }
    }

/* ======================================================================================
//### search()
// --------------------------------------------------------------------------------------
// Apply a plain text search against file names and file paths.
//
// @param `string $term`
//
// @return `JSON`
// ====================================================================================== */
    public function search($term = null)
    {
        if(is_null($term))
        {
            return Response::json('Search term required', 400);
        }
        else
        {
            return FileTable::where('File Name', 'LIKE', "%$term%")->orWhere('File Path', 'LIKE', "%$term%")->get();
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
}