<?php namespace App\Controllers\API\V1;

use App\Models\CategoryEItem;
use App\Models\Category;
use App\Models\Document;
use App\Models\EmailItem;
use BaseController, Response, Input, DB;

/* ===================================================================================================
//CategoryAPI.php
// ---------------------------------------------------------------------------------------------------
// eDiscovery Web Application
//
// [Advanced Programming Team](https://bitbucket.org/umadvprog)
//
// @authors Angela Gross, Scott Halstvedt
//
// This API helps in the generation of Category nodes along with displaying information about these
// Categories and searching within them.
// ===================================================================================================*/

class CategoryAPI extends BaseController 
{
    //////////////////////////////////////////////////////////////////////////////////////////////////

    //##### Initial collection of Nodes via POST data
    // @var `array()`
    private $Nodes = array();

    //##### Recursive accumulator of Nodes during Category and EItem combinations
    // @var `array()`
    private $nodeAcc = array();

    //##### Collection of each Category's EItems, indexed by Category ID (or hashed EItems, if it's a generated node)
    // @var `array()`
    private $categoryEItems = array();

    //////////////////////////////////////////////////////////////////////////////////////////////////

/* ===================================================================================================
//###categories()
// ---------------------------------------------------------------------------------------------------
// Grabs all eloquent model information about a specific category, the first 100
// categories if $categoryID is null and pageNum post data is unspecified, or the interval
// pageNum*100 - 99 -> pageNum*100 if page post data is specified.
//
// @param `int $categoryID`
//
// @return `JSON`
// ===================================================================================================*/
    public function categories($categoryID = null)
    {
        // If no ID is provided, grab all categories
        if (is_null($categoryID))
        {
            // Defaults to 1 if nothing is specified
            $pageNum = Input::get('page', 1);

            $categories = Category::where('Category ID', '<=', $pageNum*100)->where('Category ID', '>=', $pageNum*100 - 99)->get();

            // Even if it doesn't return anything, it still returns an array, so we have to use Eloquent's collection method, count
            if($categories->count())
            {
                return $categories;
            }
            else
            {
                return Response::json('Categories not found', 404);
            }
        }
        // Otherwise, grab specific category
        else
        {
            $category = Category::find($categoryID);

            // If the requested ID does not exist, then let the user know
            if (is_null($category))
            {
                return Response::json('Category not found', 404);
            }
            // Otherwise, return it
            else
            {
                $EItems = CategoryEItem::where('Category ID', '=', $categoryID)->get();

                $eitemsArr = array();

                foreach($EItems as $EItem)
                {
                    $eItemID = $EItem->{'E-item ID'};

                    $email = EmailItem::where('E-item ID', '=', $eItemID)->get();
                    $document = Document::where('E-item ID', '=', $eItemID)->get();


                    if($email->count())
                    {
                        $eitemsArr['emails'][] = $email->toArray();
                    }
                    else if($document->count())
                    {
                        $eitemsArr['documents'][] = $document->toArray();
                    }

                }

                $formatCategory = array
                (
                    "Category ID" => $categoryID,
                    "Num EItems" => is_object($EItems) ? $EItems->count() : 0,
                    'Category String' => $category->{'Category String'},
                    'EItems' => $eitemsArr
                );

                return $formatCategory;
            }
        }
    }

/* ======================================================================================
//### getNumCategories()
// --------------------------------------------------------------------------------------
// Returns the number of categories in the table in JSON. Uses method that grabs the
// table's name to ensure that the method will still work even if we change the table
// name (it's non-static, however, so we have to briefly make a new instance)
//
// @return `JSON`
// ======================================================================================*/
    public function getNumCategories()
    {
        return json_encode(DB::table((new Category())->getTable())->count());
    }

/* ======================================================================================
//### search()
// --------------------------------------------------------------------------------------
// Apply a plain text search against the category string.
//
// @param `string $term`
//
// @return `JSON`
// ======================================================================================*/
    public function search($term = null)
    {
        if(is_null($term))
        {
            return Response::json('Search term required', 400);
        }
        else
        {
            return Category::where('Category String', 'LIKE', "%$term%")->select(array('Category ID', 'Category String', 'E-item Count'))->get();
        }
    }

/* ======================================================================================
//### graphCategories()
// --------------------------------------------------------------------------------------
// Method that will return an array of the categories that are selected with the following
// information:
*/
// `'Is Generated'` => `1` if the node is dynamically generated, or `0` if it's in the DB,
// `'Is Selected'` => `1` if the node was selected by user, or `0` if it was recently generated,
// `'Node ID'` => ID of the node. It is generally just the category ID in DB,
// `'Category String'` => Name of category,
// `'Num EItems'` => Number of EItems that this category has associated with it,
// `'EItems'` => Array of EItem IDs,
// `'Categories'` => Array of Categories (if it is generated)
//
// It will grab all unique combinations of EItem intersections.
//
// @return `JSON`
// ======================================================================================
    public function graphCategories()
    {
        // Get category IDs from post data - expecting comma delineated IDs
        $categoryIDsPost = Input::get('selectedCategories');
        $selectedCategories = explode(",", $categoryIDsPost);

        // Get generated categories from post data - expecting the same format as above
        $generatedPost = Input::get('selectedGenerated');
        $selectedGenerated = json_decode( $generatedPost );

        // Add selected nodes to $this->Nodes[]
        $this->addSelectedNodes($selectedCategories, $selectedGenerated);

        // Generate nodes
        for($i = 0; $i < count($this->Nodes); $i++)
        {
            array_push($this->nodeAcc, $this->Nodes[$i]);
            $this->generateNodes($this->Nodes[$i], $i + 1);
        }

        // Gather and return all category nodes and generated nodes with unique EItem combinations
        return $this->getUniqueNodes();
    }

/* ======================================================================================
//### generateNodes()
// --------------------------------------------------------------------------------------
// Recursive method that helps find all combinations of EItems
//
// @param `array() $node`
//
// @param `int $count`
//
// @return `void`
// ======================================================================================*/
    private function generateNodes($node, $count)
    {
        for($j = $count; $j < count($this->Nodes); $j++)
        {
            $tempNode = $this->merge($node, $this->Nodes[$j]);

            if(isset($tempNode))
                array_push($this->nodeAcc, $tempNode);

            $this->generateNodes($tempNode, $j + 1);
        }
    }

/* ======================================================================================
//### merge()
// --------------------------------------------------------------------------------------
// Helps the recursive method join together two nodes by seeing if they have any eItems
// in common.
//
// @param `array() $node1`
//
// @param `array() $node2`
//
// @return `array()`
// ======================================================================================*/
    private function merge($node1, $node2)
    {
        $tempNode = null;

        if(isset($node1) && isset($node2))
        {
            // Grab eItemIDS from each node
            $eItems1 = $this->categoryEItems[$node1['Category ID']];
            $eItems2 = $this->categoryEItems[$node2['Category ID']];

            // Find out which ones they have in common
            $intersect = array_intersect($eItems1, $eItems2);

            // Do they have anything in common?
            if(count($intersect > 0))
            {
                // Don't include the recently generated nodes
                // (if they are recently generated then they are not selected)
                if($node1['Is Selected'] == 1 && $node2['Is Selected'] == 0)
                {
                    $nodeIDs = array($node1['Category ID']);
                }
                else if($node1['Is Selected'] == 0 && $node2['Is Selected'] == 1)
                {
                    $nodeIDs = array($node2['Category ID']);
                }
                else if($node1['Is Selected'] == 0 && $node2['Is Selected'] == 0)
                {
                    $nodeIDs = array();
                }
                else
                {
                    $nodeIDs = array($node1['Category ID'], $node2['Category ID']);
                }

                // Get all relevant, distinct category IDs together
                $categories = array_merge($node1['Categories'], $node2['Categories']);
                $categories = array_merge($categories, $nodeIDs);
                $categories = array_unique($categories);

                // Create node ID by hashing unique EItem and Category arrays
                $ID = md5(json_encode($intersect).json_encode($categories));

                // Create node if more categories
                $tempNode = array
                (
                    'Is Generated' => 1,
                    'Is Selected' => 0,
                    'Category ID' => $ID,
                    'Category String' => $node1['Category String'] . ' ? ' . $node2['Category String'],
                    'Num EItems' => count($intersect),
                    'EItems' => $intersect,
                    'Categories' => $categories
                );

                // Create EItems
                $this->categoryEItems[$ID] = $intersect;
            }
        }

        return $tempNode;
    }

/* ======================================================================================
//### addSelectedNodes()
// --------------------------------------------------------------------------------------
// Puts selected categories/generated nodes into useful data structure that will greatly
// aid in the generation of nodes.
//
// @param `array() $selectedCategories`
//
// @param `array() $selectedGenerated`
//
// @return `void`
// ======================================================================================*/
    private function addSelectedNodes($selectedCategories, $selectedGenerated)
    {
        // Add selected categories to nodes array
        foreach($selectedCategories as $categoryID)
        {
            // Get EItems and add them to global variable
            $eItems = $this->getEItems($categoryID);

            $myCategory = Category::find($categoryID);

            $this->Nodes[] = array
            (
                'Is Generated' => 0,
                'Is Selected' => 1,
                'Category ID' => $categoryID,
                'Category String' => $myCategory->{'Category String'},
                'Num EItems' => isset($eItems) ? count($eItems) : 0,
                'EItems' => $eItems,
                'Categories' => array()
            );
        }

        // Add selected generated nodes to nodes array
        foreach($selectedGenerated as $generatedNode)
        {
            $categoryID = $generatedNode['Category ID'];
            $EItems = $generatedNode['EItems'];

            // Put EItems into global variable
            if(isset($EItems) && isset($categoryID))
            {
                $this->categoryEItems[$categoryID] = $EItems;
            }

            // Add node to array
            $this->Nodes[] = $generatedNode;
        }
    }

/* ======================================================================================
//### getEItems()
// --------------------------------------------------------------------------------------
// Finds EItems for a given category (NOT generated), adds them to a global variable to
// help later node generation, and returns an array of EItems.
//
// @param `int $categoryID`
// @return `array()`
// ======================================================================================*/
    private function getEItems($categoryID)
    {
        // Find EItem
        $EItems = CategoryEItem::where('Category ID', '=', $categoryID)->get();

        $eItemsArr = array();

        if(isset($EItems))
        {
            // Grabs each EItem and puts it into an array
            foreach($EItems as $eItem)
            {
                $this->categoryEItems[$categoryID][] = $eItem->{'E-item ID'};
                $eItemsArr[] = $eItem->{'E-item ID'};
            }
        }

        return $eItemsArr;
    }

/* ======================================================================================
//### getUniqueNodes()
// --------------------------------------------------------------------------------------
// Finds generated nodes that have NOT been selected and only show the generated nodes,
// or intersections of categories, with the most categories.
//
// The "most categories" condition is important because the node generation considers
// ALL combinations of EItems and Categories. We ONLY want the UNIQUE EITEM combinations
// (so only ONE generated node for a combination of EItems) that has the MOST categories.
// This ensures that we do not see extraneous combinations that are useless to the user.
//
// @return `JSON`
// ======================================================================================*/
    private function getUniqueNodes()
    {
        $generatedCombinations = array();
        $finalNodes = array();

        // Get the nodes along with the generated nodes - separate them out for later sorting
        foreach($this->nodeAcc as $item)
        {
            // Make each item a stdClass object
            $item = (object)$item;

            // If it's generated and not selected, hash the eItems to index them by EItem
            if($item->{"Is Generated"} == 1 && $item->{"Is Selected"} == 0)
            {
                $generatedCombinations[md5(json_encode($item->EItems))][count($item->Categories)][] = $item;
            }
            // Else, just add it on to the array
            else
            {
                $finalNodes[] = $item;
            }
        }

        // Find the generated node with the most categories and add it to the final output
        foreach($generatedCombinations as $itemset)
        {
            // Sort based on count of categories
            krsort($itemset);

            // Remove count values as indexes
            $itemset = array_values($itemset);

            // Get the highest count of categories
            $itemset = array_values($itemset[0]);

            // Merge the highest count array with the output array
            $finalNodes = array_merge($finalNodes, $itemset);
        }

        return json_encode($finalNodes);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
}
