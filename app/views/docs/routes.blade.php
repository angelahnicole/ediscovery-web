@extends('docs.master')

@include('docs._partials.navigation')

@include('docs._partials.footer')

@section('content')

<div id="container">
    <div id="background"></div>
        <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th class="docs">
                <h1>
                    routes.php                </h1>
            </th>
            <th class="code">
            </th>
        </tr>
        </thead>
        <tbody>
                    <tr id="section-0">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-0">&#182;</a>
                    </div>
                    <p>===================================================================================================
 routes.php</p>

<hr />

<p>eDiscovery Web Application</p>

<p><a href="https://bitbucket.org/umadvprog">Advanced Programming Team</a></p>

<p><em class='docparam'>@authors</em> Angela Gross, Scott Halstvedt</p>

<p>These routes help redirect URLs to their proper places. This is used for the main page, the API,
 and for the documentation pages.
 ===================================================================================================</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre></pre></div>                </td>
            </tr>
                    <tr id="section-1">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-1">&#182;</a>
                    </div>
                    <p>////////////////////////////////////////////////////////////////////////////////////////////////////</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre></pre></div>                </td>
            </tr>
                    <tr id="section-2">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-2">&#182;</a>
                    </div>
                    <h3>MAIN PAGE of eDiscovery</h3>
                </td>
                <td class="code">
                    <div class="highlight"><pre></pre></div>                </td>
            </tr>
                    <tr id="section-3">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-3">&#182;</a>
                    </div>
                    <p>Displays the "meat" of the program, or the part that the end-user will interact with</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre><span class="nx">Route</span><span class="o">::</span><span class="na">get</span><span class="p">(</span><span class="s1">&#39;/&#39;</span><span class="p">,</span> <span class="s1">&#39;App\Controllers\MainController@showHome&#39;</span><span class="p">);</span></pre></div>                </td>
            </tr>
                    <tr id="section-4">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-4">&#182;</a>
                    </div>
                    <p>////////////////////////////////////////////////////////////////////////////////////////////////////</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre></pre></div>                </td>
            </tr>
                    <tr id="section-5">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-5">&#182;</a>
                    </div>
                    <h3>API for eDiscovery</h3>
                </td>
                <td class="code">
                    <div class="highlight"><pre></pre></div>                </td>
            </tr>
                    <tr id="section-6">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-6">&#182;</a>
                    </div>
                    <p>Used to talk between Backbone.js and the DB (and Laravel provides a layer of abstraction)</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre></pre></div>                </td>
            </tr>
                    <tr id="section-7">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-7">&#182;</a>
                    </div>
                    <hr />
                </td>
                <td class="code">
                    <div class="highlight"><pre></pre></div>                </td>
            </tr>
                    <tr id="section-8">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-8">&#182;</a>
                    </div>
                    <h4>Version 1 of the API</h4>
                </td>
                <td class="code">
                    <div class="highlight"><pre><span class="nx">Route</span><span class="o">::</span><span class="na">group</span><span class="p">(</span><span class="k">array</span><span class="p">(</span><span class="s1">&#39;prefix&#39;</span> <span class="o">=&gt;</span> <span class="s1">&#39;/api/v1/&#39;</span><span class="p">),</span> <span class="k">function</span><span class="p">()</span>
<span class="p">{</span></pre></div>                </td>
            </tr>
                    <tr id="section-9">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-9">&#182;</a>
                    </div>
                    <h5><em>DOCUMENT ROUTES</em> that help retrieve documents and their relations</h5>

<hr />
                </td>
                <td class="code">
                    <div class="highlight"><pre>    <span class="nx">Route</span><span class="o">::</span><span class="na">group</span><span class="p">(</span><span class="k">array</span><span class="p">(</span><span class="s1">&#39;prefix&#39;</span> <span class="o">=&gt;</span> <span class="s1">&#39;documents&#39;</span><span class="p">),</span> <span class="k">function</span><span class="p">()</span>
    <span class="p">{</span></pre></div>                </td>
            </tr>
                    <tr id="section-10">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-10">&#182;</a>
                    </div>
                    <p>Plain text search of file names (and paths)</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>        <span class="nx">Route</span><span class="o">::</span><span class="na">any</span><span class="p">(</span><span class="s1">&#39;/search/{term}&#39;</span><span class="p">,</span> <span class="k">array</span><span class="p">(</span><span class="s1">&#39;as&#39;</span> <span class="o">=&gt;</span> <span class="s1">&#39;api.documents.search&#39;</span><span class="p">,</span> <span class="s1">&#39;uses&#39;</span> <span class="o">=&gt;</span> <span class="s1">&#39;App\Controllers\API\V1\DocumentAPI@search&#39;</span><span class="p">));</span></pre></div>                </td>
            </tr>
                    <tr id="section-11">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-11">&#182;</a>
                    </div>
                    <p>Grab all (or a specific) document</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>        <span class="nx">Route</span><span class="o">::</span><span class="na">any</span><span class="p">(</span><span class="s1">&#39;{documentID?}&#39;</span><span class="p">,</span> <span class="k">array</span><span class="p">(</span><span class="s1">&#39;as&#39;</span> <span class="o">=&gt;</span> <span class="s1">&#39;api.documents&#39;</span><span class="p">,</span> <span class="s1">&#39;uses&#39;</span> <span class="o">=&gt;</span> <span class="s1">&#39;App\Controllers\API\V1\DocumentAPI@documents&#39;</span><span class="p">));</span>
    <span class="p">});</span></pre></div>                </td>
            </tr>
                    <tr id="section-12">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-12">&#182;</a>
                    </div>
                    <h5><em>EITEM ROUTES</em> that help retrieve eItems and their relations</h5>

<hr />
                </td>
                <td class="code">
                    <div class="highlight"><pre>    <span class="nx">Route</span><span class="o">::</span><span class="na">group</span><span class="p">(</span><span class="k">array</span><span class="p">(</span><span class="s1">&#39;prefix&#39;</span> <span class="o">=&gt;</span> <span class="s1">&#39;eitems&#39;</span><span class="p">),</span> <span class="k">function</span><span class="p">()</span>
    <span class="p">{</span></pre></div>                </td>
            </tr>
                    <tr id="section-13">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-13">&#182;</a>
                    </div>
                    <p>Grab all (or a specific) eItem</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>        <span class="nx">Route</span><span class="o">::</span><span class="na">any</span><span class="p">(</span><span class="s1">&#39;{eItemID?}&#39;</span><span class="p">,</span> <span class="k">array</span><span class="p">(</span><span class="s1">&#39;as&#39;</span> <span class="o">=&gt;</span> <span class="s1">&#39;api.eitems&#39;</span><span class="p">,</span> <span class="s1">&#39;uses&#39;</span> <span class="o">=&gt;</span> <span class="s1">&#39;App\Controllers\API\V1\EItemAPI@eItems&#39;</span><span class="p">));</span>
    <span class="p">});</span></pre></div>                </td>
            </tr>
                    <tr id="section-14">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-14">&#182;</a>
                    </div>
                    <h5><em>CATEGORY ROUTES</em> that help retrieve categories and their relations (including dynamically created intersections)</h5>

<hr />
                </td>
                <td class="code">
                    <div class="highlight"><pre>    <span class="nx">Route</span><span class="o">::</span><span class="na">group</span><span class="p">(</span><span class="k">array</span><span class="p">(</span><span class="s1">&#39;prefix&#39;</span> <span class="o">=&gt;</span> <span class="s1">&#39;categories&#39;</span><span class="p">),</span> <span class="k">function</span><span class="p">()</span>
    <span class="p">{</span></pre></div>                </td>
            </tr>
                    <tr id="section-15">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-15">&#182;</a>
                    </div>
                    <p>Plain text search of categories</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>        <span class="nx">Route</span><span class="o">::</span><span class="na">any</span><span class="p">(</span><span class="s1">&#39;/search/{term}&#39;</span><span class="p">,</span> <span class="k">array</span><span class="p">(</span><span class="s1">&#39;as&#39;</span> <span class="o">=&gt;</span> <span class="s1">&#39;api.categories.search&#39;</span><span class="p">,</span> <span class="s1">&#39;uses&#39;</span> <span class="o">=&gt;</span> <span class="s1">&#39;App\Controllers\API\V1\CategoryAPI@search&#39;</span><span class="p">));</span></pre></div>                </td>
            </tr>
                    <tr id="section-16">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-16">&#182;</a>
                    </div>
                    <p>Get the number of all categories</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>        <span class="nx">Route</span><span class="o">::</span><span class="na">any</span><span class="p">(</span><span class="s1">&#39;num&#39;</span><span class="p">,</span> <span class="k">array</span><span class="p">(</span><span class="s1">&#39;as&#39;</span> <span class="o">=&gt;</span> <span class="s1">&#39;api.categories.num&#39;</span><span class="p">,</span> <span class="s1">&#39;uses&#39;</span> <span class="o">=&gt;</span> <span class="s1">&#39;App\Controllers\API\V1\CategoryAPI@getNumCategories&#39;</span><span class="p">));</span></pre></div>                </td>
            </tr>
                    <tr id="section-17">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-17">&#182;</a>
                    </div>
                    <p>Generate nodes based on eItems</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>        <span class="nx">Route</span><span class="o">::</span><span class="na">any</span><span class="p">(</span><span class="s1">&#39;graph&#39;</span><span class="p">,</span> <span class="k">array</span><span class="p">(</span><span class="s1">&#39;as&#39;</span> <span class="o">=&gt;</span> <span class="s1">&#39;api.categories.graph&#39;</span><span class="p">,</span> <span class="s1">&#39;uses&#39;</span> <span class="o">=&gt;</span> <span class="s1">&#39;App\Controllers\API\V1\CategoryAPI@graphCategories&#39;</span><span class="p">));</span></pre></div>                </td>
            </tr>
                    <tr id="section-18">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-18">&#182;</a>
                    </div>
                    <p>Grab all (or a specific) category</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>        <span class="nx">Route</span><span class="o">::</span><span class="na">any</span><span class="p">(</span><span class="s1">&#39;{categoryID?}&#39;</span><span class="p">,</span> <span class="k">array</span><span class="p">(</span><span class="s1">&#39;as&#39;</span> <span class="o">=&gt;</span> <span class="s1">&#39;api.categories&#39;</span><span class="p">,</span> <span class="s1">&#39;uses&#39;</span> <span class="o">=&gt;</span> <span class="s1">&#39;App\Controllers\API\V1\CategoryAPI@categories&#39;</span><span class="p">));</span>
    <span class="p">});</span>
<span class="p">});</span></pre></div>                </td>
            </tr>
                    <tr id="section-19">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-19">&#182;</a>
                    </div>
                    <p>////////////////////////////////////////////////////////////////////////////////////////////////////</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre></pre></div>                </td>
            </tr>
                    <tr id="section-20">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-20">&#182;</a>
                    </div>
                    <h3>Documentation for eDiscovery</h3>
                </td>
                <td class="code">
                    <div class="highlight"><pre></pre></div>                </td>
            </tr>
                    <tr id="section-21">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-21">&#182;</a>
                    </div>
                    <p>Used to view documentation with <a href="http:phrocco.info/">Phrocco</a>, a documentation generator
 for PHP.</p>

<hr />
                </td>
                <td class="code">
                    <div class="highlight"><pre><span class="nx">Route</span><span class="o">::</span><span class="na">group</span><span class="p">(</span><span class="k">array</span><span class="p">(</span><span class="s1">&#39;prefix&#39;</span> <span class="o">=&gt;</span> <span class="s1">&#39;docs&#39;</span><span class="p">),</span> <span class="k">function</span><span class="p">()</span>
<span class="p">{</span></pre></div>                </td>
            </tr>
                    <tr id="section-22">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-22">&#182;</a>
                    </div>
                    <h5><em>HOME DOC ROUTE</em> that takes the user to our documentation index</h5>

<hr />
                </td>
                <td class="code">
                    <div class="highlight"><pre>   <span class="nx">Route</span><span class="o">::</span><span class="na">any</span><span class="p">(</span><span class="s1">&#39;/&#39;</span><span class="p">,</span> <span class="k">function</span><span class="p">(){</span> <span class="k">return</span> <span class="nx">View</span><span class="o">::</span><span class="na">make</span><span class="p">(</span><span class="s1">&#39;docs.index&#39;</span><span class="p">);})</span> <span class="p">;</span></pre></div>                </td>
            </tr>
                    <tr id="section-23">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-23">&#182;</a>
                    </div>
                    <h5><em>DOC ROUTE</em> that documents this very file, routes.php</h5>

<hr />
                </td>
                <td class="code">
                    <div class="highlight"><pre>    <span class="nx">Route</span><span class="o">::</span><span class="na">any</span><span class="p">(</span><span class="s1">&#39;/routes&#39;</span><span class="p">,</span> <span class="k">function</span><span class="p">(){</span> <span class="k">return</span> <span class="nx">View</span><span class="o">::</span><span class="na">make</span><span class="p">(</span><span class="s1">&#39;docs.routes&#39;</span><span class="p">);})</span> <span class="p">;</span></pre></div>                </td>
            </tr>
                    <tr id="section-24">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-24">&#182;</a>
                    </div>
                    <h5><em>MODEL DOC ROUTES</em> that document all database models in the application</h5>

<hr />
                </td>
                <td class="code">
                    <div class="highlight"><pre>    <span class="nx">Route</span><span class="o">::</span><span class="na">group</span><span class="p">(</span><span class="k">array</span><span class="p">(</span><span class="s1">&#39;prefix&#39;</span> <span class="o">=&gt;</span> <span class="s1">&#39;models&#39;</span><span class="p">),</span> <span class="k">function</span><span class="p">()</span>
    <span class="p">{</span>
        <span class="nx">Route</span><span class="o">::</span><span class="na">any</span><span class="p">(</span><span class="s1">&#39;&#39;</span><span class="p">,</span> <span class="k">function</span><span class="p">(){</span> <span class="k">return</span> <span class="nx">View</span><span class="o">::</span><span class="na">make</span><span class="p">(</span><span class="s1">&#39;docs.models.Category&#39;</span><span class="p">);})</span> <span class="p">;</span>
        <span class="nx">Route</span><span class="o">::</span><span class="na">any</span><span class="p">(</span><span class="s1">&#39;Category&#39;</span><span class="p">,</span> <span class="k">function</span><span class="p">(){</span> <span class="k">return</span> <span class="nx">View</span><span class="o">::</span><span class="na">make</span><span class="p">(</span><span class="s1">&#39;docs.models.Category&#39;</span><span class="p">);})</span> <span class="p">;</span>
        <span class="nx">Route</span><span class="o">::</span><span class="na">any</span><span class="p">(</span><span class="s1">&#39;CategoryEItem&#39;</span><span class="p">,</span> <span class="k">function</span><span class="p">(){</span> <span class="k">return</span> <span class="nx">View</span><span class="o">::</span><span class="na">make</span><span class="p">(</span><span class="s1">&#39;docs.models.CategoryEItem&#39;</span><span class="p">);})</span> <span class="p">;</span>
        <span class="nx">Route</span><span class="o">::</span><span class="na">any</span><span class="p">(</span><span class="s1">&#39;Document&#39;</span><span class="p">,</span> <span class="k">function</span><span class="p">(){</span> <span class="k">return</span> <span class="nx">View</span><span class="o">::</span><span class="na">make</span><span class="p">(</span><span class="s1">&#39;docs.models.Document&#39;</span><span class="p">);})</span> <span class="p">;</span>
        <span class="nx">Route</span><span class="o">::</span><span class="na">any</span><span class="p">(</span><span class="s1">&#39;EItem&#39;</span><span class="p">,</span> <span class="k">function</span><span class="p">(){</span> <span class="k">return</span> <span class="nx">View</span><span class="o">::</span><span class="na">make</span><span class="p">(</span><span class="s1">&#39;docs.models.EItem&#39;</span><span class="p">);})</span> <span class="p">;</span>
        <span class="nx">Route</span><span class="o">::</span><span class="na">any</span><span class="p">(</span><span class="s1">&#39;EmailItem&#39;</span><span class="p">,</span> <span class="k">function</span><span class="p">(){</span> <span class="k">return</span> <span class="nx">View</span><span class="o">::</span><span class="na">make</span><span class="p">(</span><span class="s1">&#39;docs.models.EmailItem&#39;</span><span class="p">);})</span> <span class="p">;</span>
        <span class="nx">Route</span><span class="o">::</span><span class="na">any</span><span class="p">(</span><span class="s1">&#39;FileTable&#39;</span><span class="p">,</span> <span class="k">function</span><span class="p">(){</span> <span class="k">return</span> <span class="nx">View</span><span class="o">::</span><span class="na">make</span><span class="p">(</span><span class="s1">&#39;docs.models.FileTable&#39;</span><span class="p">);})</span> <span class="p">;</span>

    <span class="p">});</span></pre></div>                </td>
            </tr>
                    <tr id="section-25">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-25">&#182;</a>
                    </div>
                    <h5><em>API DOC ROUTES</em> that document all API Controllers in the application</h5>

<hr />
                </td>
                <td class="code">
                    <div class="highlight"><pre>    <span class="nx">Route</span><span class="o">::</span><span class="na">group</span><span class="p">(</span><span class="k">array</span><span class="p">(</span><span class="s1">&#39;prefix&#39;</span> <span class="o">=&gt;</span> <span class="s1">&#39;API&#39;</span><span class="p">),</span> <span class="k">function</span><span class="p">()</span>
    <span class="p">{</span>
        <span class="nx">Route</span><span class="o">::</span><span class="na">any</span><span class="p">(</span><span class="s1">&#39;&#39;</span><span class="p">,</span> <span class="k">function</span><span class="p">(){</span> <span class="k">return</span> <span class="nx">View</span><span class="o">::</span><span class="na">make</span><span class="p">(</span><span class="s1">&#39;docs.API.CategoryAPI&#39;</span><span class="p">);})</span> <span class="p">;</span>
        <span class="nx">Route</span><span class="o">::</span><span class="na">any</span><span class="p">(</span><span class="s1">&#39;CategoryAPI&#39;</span><span class="p">,</span> <span class="k">function</span><span class="p">(){</span> <span class="k">return</span> <span class="nx">View</span><span class="o">::</span><span class="na">make</span><span class="p">(</span><span class="s1">&#39;docs.API.CategoryAPI&#39;</span><span class="p">);})</span> <span class="p">;</span>
        <span class="nx">Route</span><span class="o">::</span><span class="na">any</span><span class="p">(</span><span class="s1">&#39;DocumentAPI&#39;</span><span class="p">,</span> <span class="k">function</span><span class="p">(){</span> <span class="k">return</span> <span class="nx">View</span><span class="o">::</span><span class="na">make</span><span class="p">(</span><span class="s1">&#39;docs.API.DocumentAPI&#39;</span><span class="p">);})</span> <span class="p">;</span>
        <span class="nx">Route</span><span class="o">::</span><span class="na">any</span><span class="p">(</span><span class="s1">&#39;EItemAPI&#39;</span><span class="p">,</span> <span class="k">function</span><span class="p">(){</span> <span class="k">return</span> <span class="nx">View</span><span class="o">::</span><span class="na">make</span><span class="p">(</span><span class="s1">&#39;docs.API.EItemAPI&#39;</span><span class="p">);})</span> <span class="p">;</span>


    <span class="p">});</span>

<span class="p">});</span></pre></div>                </td>
            </tr>
                    <tr id="section-26">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-26">&#182;</a>
                    </div>
                    <p>////////////////////////////////////////////////////////////////////////////////////////////////////</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre></pre></div></pre></div>                </td>
            </tr>
                </tbody>
    </table>
</div>

@stop