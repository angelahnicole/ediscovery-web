@extends('docs.master')

@include('docs._partials.navigation')

@include('docs._partials.footer')

@section('content')

<div id="container">
    <div id="background"></div>
                    <div id="jump_to">
            Jump To &hellip;
            <div id="jump_wrapper">
                <div id="jump_page">
                    <ul>
                                                                                                                        <li></li>                                                <li><a class="source level_0" href="http://192.81.129.120:8080/docs/api/DocumentAPI">
                                                                        DocumentAPI.php                                    </a></li>
                                                                                                                                <li></li>                                                <li><a class="source level_0" href="http://192.81.129.120:8080/docs/api/CategoryAPI">
                                                                        CategoryAPI.php                                    </a></li>
                                                                                                                                <li></li>                                                <li><a class="source level_0" href="http://192.81.129.120:8080/docs/api/EItemAPI">
                                                                        EItemAPI.php                                    </a></li>
                                                    </ul>
                </div>
            </div>
        </div>
        <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th class="docs">
                <h1>
                    CategoryAPI.php                </h1>
            </th>
            <th class="code">
            </th>
        </tr>
        </thead>
        <tbody>
                    <tr id="section-0">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-0">&#182;</a>
                    </div>
                    <p>===================================================================================================
CategoryAPI.php</p>

<hr />

<p>eDiscovery Web Application</p>

<p><a href="https://bitbucket.org/umadvprog">Advanced Programming Team</a></p>

<p><em class='docparam'>@authors</em> Angela Gross, Scott Halstvedt</p>

<p>This API helps in the generation of Category nodes along with displaying information about these
 Categories and searching within them.
 ===================================================================================================</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre><span class="k">class</span> <span class="nc">CategoryAPI</span> <span class="k">extends</span> <span class="nx">BaseController</span> 
<span class="p">{</span></pre></div>                </td>
            </tr>
                    <tr id="section-1">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-1">&#182;</a>
                    </div>
                    <p>////////////////////////////////////////////////////////////////////////////////////////////////</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre></pre></div>                </td>
            </tr>
                    <tr id="section-2">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-2">&#182;</a>
                    </div>
                    <h5>Initial collection of Nodes via POST data</h5>
                </td>
                <td class="code">
                    <div class="highlight"><pre></pre></div>                </td>
            </tr>
                    <tr id="section-3">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-3">&#182;</a>
                    </div>
                    <p><em class='docparam'>@var</em> <code>array()</code></p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>    <span class="k">private</span> <span class="nv">$Nodes</span> <span class="o">=</span> <span class="k">array</span><span class="p">();</span></pre></div>                </td>
            </tr>
                    <tr id="section-4">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-4">&#182;</a>
                    </div>
                    <h5>Recursive accumulator of Nodes during Category and EItem combinations</h5>
                </td>
                <td class="code">
                    <div class="highlight"><pre></pre></div>                </td>
            </tr>
                    <tr id="section-5">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-5">&#182;</a>
                    </div>
                    <p><em class='docparam'>@var</em> <code>array()</code></p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>    <span class="k">private</span> <span class="nv">$nodeAcc</span> <span class="o">=</span> <span class="k">array</span><span class="p">();</span></pre></div>                </td>
            </tr>
                    <tr id="section-6">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-6">&#182;</a>
                    </div>
                    <h5>Collection of each Category's EItems, indexed by Category ID (or hashed EItems, if it's a generated node)</h5>
                </td>
                <td class="code">
                    <div class="highlight"><pre></pre></div>                </td>
            </tr>
                    <tr id="section-7">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-7">&#182;</a>
                    </div>
                    <p><em class='docparam'>@var</em> <code>array()</code></p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>    <span class="k">private</span> <span class="nv">$categoryEItems</span> <span class="o">=</span> <span class="k">array</span><span class="p">();</span></pre></div>                </td>
            </tr>
                    <tr id="section-8">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-8">&#182;</a>
                    </div>
                    <p>////////////////////////////////////////////////////////////////////////////////////////////////</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre></pre></div>                </td>
            </tr>
                    <tr id="section-9">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-9">&#182;</a>
                    </div>
                    <p>===================================================================================================</p>

<h3>categories()</h3>

<hr />

<p>Grabs all eloquent model information about a specific category, the first 100
 categories if $categoryID is null and pageNum post data is unspecified, or the interval
 pageNum100 - 99 -> pageNum*100 if page post data is specified.</p>

<p><em class='docparam'>@param</em> <code>int $categoryID</code></p>

<p><em class='docparam'>@return</em> <code>JSON</code>
 ===================================================================================================</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>    <span class="k">public</span> <span class="k">function</span> <span class="nf">categories</span><span class="p">(</span><span class="nv">$categoryID</span> <span class="o">=</span> <span class="k">null</span><span class="p">)</span>
    <span class="p">{</span></pre></div>                </td>
            </tr>
                    <tr id="section-10">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-10">&#182;</a>
                    </div>
                    <p>If no ID is provided, grab all categories</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>        <span class="k">if</span> <span class="p">(</span><span class="nb">is_null</span><span class="p">(</span><span class="nv">$categoryID</span><span class="p">))</span>
        <span class="p">{</span></pre></div>                </td>
            </tr>
                    <tr id="section-11">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-11">&#182;</a>
                    </div>
                    <p>Defaults to 1 if nothing is specified</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>            <span class="nv">$pageNum</span> <span class="o">=</span> <span class="nx">Input</span><span class="o">::</span><span class="na">get</span><span class="p">(</span><span class="s1">&#39;page&#39;</span><span class="p">,</span> <span class="mi">1</span><span class="p">);</span>

            <span class="nv">$categories</span> <span class="o">=</span> <span class="nx">Category</span><span class="o">::</span><span class="na">where</span><span class="p">(</span><span class="s1">&#39;Category ID&#39;</span><span class="p">,</span> <span class="s1">&#39;&lt;=&#39;</span><span class="p">,</span> <span class="nv">$pageNum</span><span class="o">*</span><span class="mi">100</span><span class="p">)</span><span class="o">-&gt;</span><span class="na">where</span><span class="p">(</span><span class="s1">&#39;Category ID&#39;</span><span class="p">,</span> <span class="s1">&#39;&gt;=&#39;</span><span class="p">,</span> <span class="nv">$pageNum</span><span class="o">*</span><span class="mi">100</span> <span class="o">-</span> <span class="mi">99</span><span class="p">)</span><span class="o">-&gt;</span><span class="na">get</span><span class="p">();</span></pre></div>                </td>
            </tr>
                    <tr id="section-12">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-12">&#182;</a>
                    </div>
                    <p>Even if it doesn't return anything, it still returns an array, so we have to use Eloquent's collection method, count</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>            <span class="k">if</span><span class="p">(</span><span class="nv">$categories</span><span class="o">-&gt;</span><span class="na">count</span><span class="p">())</span>
            <span class="p">{</span>
                <span class="k">return</span> <span class="nv">$categories</span><span class="p">;</span>
            <span class="p">}</span>
            <span class="k">else</span>
            <span class="p">{</span>
                <span class="k">return</span> <span class="nx">Response</span><span class="o">::</span><span class="na">json</span><span class="p">(</span><span class="s1">&#39;Categories not found&#39;</span><span class="p">,</span> <span class="mi">404</span><span class="p">);</span>
            <span class="p">}</span>
        <span class="p">}</span></pre></div>                </td>
            </tr>
                    <tr id="section-13">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-13">&#182;</a>
                    </div>
                    <p>Otherwise, grab specific category</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>        <span class="k">else</span>
        <span class="p">{</span>
            <span class="nv">$category</span> <span class="o">=</span> <span class="nx">Category</span><span class="o">::</span><span class="na">find</span><span class="p">(</span><span class="nv">$categoryID</span><span class="p">);</span></pre></div>                </td>
            </tr>
                    <tr id="section-14">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-14">&#182;</a>
                    </div>
                    <p>If the requested ID does not exist, then let the user know</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>            <span class="k">if</span> <span class="p">(</span><span class="nb">is_null</span><span class="p">(</span><span class="nv">$category</span><span class="p">))</span>
            <span class="p">{</span>
                <span class="k">return</span> <span class="nx">Response</span><span class="o">::</span><span class="na">json</span><span class="p">(</span><span class="s1">&#39;Category not found&#39;</span><span class="p">,</span> <span class="mi">404</span><span class="p">);</span>
            <span class="p">}</span></pre></div>                </td>
            </tr>
                    <tr id="section-15">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-15">&#182;</a>
                    </div>
                    <p>Otherwise, return it</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>            <span class="k">else</span>
            <span class="p">{</span>
                <span class="nv">$EItems</span> <span class="o">=</span> <span class="nx">CategoryEItem</span><span class="o">::</span><span class="na">where</span><span class="p">(</span><span class="s1">&#39;Category ID&#39;</span><span class="p">,</span> <span class="s1">&#39;=&#39;</span><span class="p">,</span> <span class="nv">$categoryID</span><span class="p">)</span><span class="o">-&gt;</span><span class="na">get</span><span class="p">();</span>

                <span class="nv">$eitemsArr</span> <span class="o">=</span> <span class="k">array</span><span class="p">();</span>

                <span class="k">foreach</span><span class="p">(</span><span class="nv">$EItems</span> <span class="k">as</span> <span class="nv">$EItem</span><span class="p">)</span>
                <span class="p">{</span>
                    <span class="nv">$eItemID</span> <span class="o">=</span> <span class="nv">$EItem</span><span class="o">-&gt;</span><span class="p">{</span><span class="s1">&#39;E-item ID&#39;</span><span class="p">};</span>

                    <span class="nv">$email</span> <span class="o">=</span> <span class="nx">EmailItem</span><span class="o">::</span><span class="na">where</span><span class="p">(</span><span class="s1">&#39;E-item ID&#39;</span><span class="p">,</span> <span class="s1">&#39;=&#39;</span><span class="p">,</span> <span class="nv">$eItemID</span><span class="p">)</span><span class="o">-&gt;</span><span class="na">get</span><span class="p">();</span>
                    <span class="nv">$document</span> <span class="o">=</span> <span class="nx">Document</span><span class="o">::</span><span class="na">where</span><span class="p">(</span><span class="s1">&#39;E-item ID&#39;</span><span class="p">,</span> <span class="s1">&#39;=&#39;</span><span class="p">,</span> <span class="nv">$eItemID</span><span class="p">)</span><span class="o">-&gt;</span><span class="na">get</span><span class="p">();</span>


                    <span class="k">if</span><span class="p">(</span><span class="nv">$email</span><span class="o">-&gt;</span><span class="na">count</span><span class="p">())</span>
                    <span class="p">{</span>
                        <span class="nv">$eitemsArr</span><span class="p">[</span><span class="s1">&#39;emails&#39;</span><span class="p">][]</span> <span class="o">=</span> <span class="nv">$email</span><span class="o">-&gt;</span><span class="na">toArray</span><span class="p">();</span>
                    <span class="p">}</span>
                    <span class="k">else</span> <span class="k">if</span><span class="p">(</span><span class="nv">$document</span><span class="o">-&gt;</span><span class="na">count</span><span class="p">())</span>
                    <span class="p">{</span>
                        <span class="nv">$eitemsArr</span><span class="p">[</span><span class="s1">&#39;documents&#39;</span><span class="p">][]</span> <span class="o">=</span> <span class="nv">$document</span><span class="o">-&gt;</span><span class="na">toArray</span><span class="p">();</span>
                    <span class="p">}</span>

                <span class="p">}</span>

                <span class="nv">$formatCategory</span> <span class="o">=</span> <span class="k">array</span>
                <span class="p">(</span>
                    <span class="s2">&quot;Category ID&quot;</span> <span class="o">=&gt;</span> <span class="nv">$categoryID</span><span class="p">,</span>
                    <span class="s2">&quot;Num EItems&quot;</span> <span class="o">=&gt;</span> <span class="nb">is_object</span><span class="p">(</span><span class="nv">$EItems</span><span class="p">)</span> <span class="o">?</span> <span class="nv">$EItems</span><span class="o">-&gt;</span><span class="na">count</span><span class="p">()</span> <span class="o">:</span> <span class="mi">0</span><span class="p">,</span>
                    <span class="s1">&#39;Category String&#39;</span> <span class="o">=&gt;</span> <span class="nv">$category</span><span class="o">-&gt;</span><span class="p">{</span><span class="s1">&#39;Category String&#39;</span><span class="p">},</span>
                    <span class="s1">&#39;EItems&#39;</span> <span class="o">=&gt;</span> <span class="nv">$eitemsArr</span>
                <span class="p">);</span>

                <span class="k">return</span> <span class="nv">$formatCategory</span><span class="p">;</span>
            <span class="p">}</span>
        <span class="p">}</span>
    <span class="p">}</span></pre></div>                </td>
            </tr>
                    <tr id="section-16">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-16">&#182;</a>
                    </div>
                    <p>======================================================================================
   ### getNumCategories()
    --------------------------------------------------------------------------------------
    Returns the number of categories in the table in JSON. Uses method that grabs the
    table's name to ensure that the method will still work even if we change the table
    name (it's non-static, however, so we have to briefly make a new instance)</p>

<p><em class='docparam'>@return</em> <code>JSON</code>
    ======================================================================================</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>    <span class="k">public</span> <span class="k">function</span> <span class="nf">getNumCategories</span><span class="p">()</span>
    <span class="p">{</span>
        <span class="k">return</span> <span class="nb">json_encode</span><span class="p">(</span><span class="nx">DB</span><span class="o">::</span><span class="na">table</span><span class="p">((</span><span class="k">new</span> <span class="nx">Category</span><span class="p">())</span><span class="o">-&gt;</span><span class="na">getTable</span><span class="p">())</span><span class="o">-&gt;</span><span class="na">count</span><span class="p">());</span>
    <span class="p">}</span></pre></div>                </td>
            </tr>
                    <tr id="section-17">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-17">&#182;</a>
                    </div>
                    <p>======================================================================================
    ### search()
     --------------------------------------------------------------------------------------
     Apply a plain text search against the category string.</p>

<pre><code>&lt;em class='docparam'&gt;@param&lt;/em&gt; `string $term`
&lt;em class='docparam'&gt;@return&lt;/em&gt; `JSON`
 ======================================================================================
</code></pre>
                </td>
                <td class="code">
                    <div class="highlight"><pre>    <span class="k">public</span> <span class="k">function</span> <span class="nf">search</span><span class="p">(</span><span class="nv">$term</span> <span class="o">=</span> <span class="k">null</span><span class="p">)</span>
    <span class="p">{</span>
        <span class="k">if</span><span class="p">(</span><span class="nb">is_null</span><span class="p">(</span><span class="nv">$term</span><span class="p">))</span>
        <span class="p">{</span>
            <span class="k">return</span> <span class="nx">Response</span><span class="o">::</span><span class="na">json</span><span class="p">(</span><span class="s1">&#39;Search term required&#39;</span><span class="p">,</span> <span class="mi">400</span><span class="p">);</span>
        <span class="p">}</span>
        <span class="k">else</span>
        <span class="p">{</span>
            <span class="k">return</span> <span class="nx">Category</span><span class="o">::</span><span class="na">where</span><span class="p">(</span><span class="s1">&#39;Category String&#39;</span><span class="p">,</span> <span class="s1">&#39;LIKE&#39;</span><span class="p">,</span> <span class="s2">&quot;%</span><span class="si">$term</span><span class="s2">%&quot;</span><span class="p">)</span><span class="o">-&gt;</span><span class="na">select</span><span class="p">(</span><span class="k">array</span><span class="p">(</span><span class="s1">&#39;Category ID&#39;</span><span class="p">,</span> <span class="s1">&#39;Category String&#39;</span><span class="p">,</span> <span class="s1">&#39;E-item Count&#39;</span><span class="p">))</span><span class="o">-&gt;</span><span class="na">get</span><span class="p">();</span>
        <span class="p">}</span>
    <span class="p">}</span></pre></div>                </td>
            </tr>
                    <tr id="section-18">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-18">&#182;</a>
                    </div>
                    <p>======================================================================================</p>

<h3>graphCategories()</h3>

<hr />

<p>Method that will return an array of the categories that are selected with the following
 information:</p>

<pre><code>'Is Generated' =&gt; 1 if the node is dynamically generated, or 0 if it's in the DB,
'Is Selected' =&gt; 1 if the node was selected by user, or 0 if it was recently generated,
'Node ID' =&gt; ID of the node. It is generally just the category node,
'Category String' =&gt; Name of category,
'Num EItems' =&gt; Number of EItems that this category pertains to,
'EItems' =&gt; Array of EItem IDs,
'Categories' =&gt; Array of Categories, if it is generated
</code></pre>

<p>It will grab all unique combinations of EItem intersections.</p>

<p><em class='docparam'>@return</em> <code>JSON</code>
 ======================================================================================</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>    <span class="k">public</span> <span class="k">function</span> <span class="nf">graphCategories</span><span class="p">()</span>
    <span class="p">{</span></pre></div>                </td>
            </tr>
                    <tr id="section-19">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-19">&#182;</a>
                    </div>
                    <p>Get category IDs from post data - expecting comma delineated IDs</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>        <span class="nv">$categoryIDsPost</span> <span class="o">=</span> <span class="nx">Input</span><span class="o">::</span><span class="na">get</span><span class="p">(</span><span class="s1">&#39;selectedCategories&#39;</span><span class="p">);</span>
        <span class="nv">$selectedCategories</span> <span class="o">=</span> <span class="nb">explode</span><span class="p">(</span><span class="s2">&quot;,&quot;</span><span class="p">,</span> <span class="nv">$categoryIDsPost</span><span class="p">);</span></pre></div>                </td>
            </tr>
                    <tr id="section-20">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-20">&#182;</a>
                    </div>
                    <p>Get generated categories from post data - expecting the same format as above</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>        <span class="nv">$generatedPost</span> <span class="o">=</span> <span class="nx">Input</span><span class="o">::</span><span class="na">get</span><span class="p">(</span><span class="s1">&#39;selectedGenerated&#39;</span><span class="p">);</span>
        <span class="nv">$selectedGenerated</span> <span class="o">=</span> <span class="nb">json_decode</span><span class="p">(</span> <span class="nv">$generatedPost</span> <span class="p">);</span></pre></div>                </td>
            </tr>
                    <tr id="section-21">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-21">&#182;</a>
                    </div>
                    <p>Add selected nodes to $this->Nodes[]</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>        <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">addSelectedNodes</span><span class="p">(</span><span class="nv">$selectedCategories</span><span class="p">,</span> <span class="nv">$selectedGenerated</span><span class="p">);</span></pre></div>                </td>
            </tr>
                    <tr id="section-22">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-22">&#182;</a>
                    </div>
                    <p>Generate nodes</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>        <span class="k">for</span><span class="p">(</span><span class="nv">$i</span> <span class="o">=</span> <span class="mi">0</span><span class="p">;</span> <span class="nv">$i</span> <span class="o">&lt;</span> <span class="nb">count</span><span class="p">(</span><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">Nodes</span><span class="p">);</span> <span class="nv">$i</span><span class="o">++</span><span class="p">)</span>
        <span class="p">{</span>
            <span class="nb">array_push</span><span class="p">(</span><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">nodeAcc</span><span class="p">,</span> <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">Nodes</span><span class="p">[</span><span class="nv">$i</span><span class="p">]);</span>
            <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">generateNodes</span><span class="p">(</span><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">Nodes</span><span class="p">[</span><span class="nv">$i</span><span class="p">],</span> <span class="nv">$i</span> <span class="o">+</span> <span class="mi">1</span><span class="p">);</span>
        <span class="p">}</span></pre></div>                </td>
            </tr>
                    <tr id="section-23">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-23">&#182;</a>
                    </div>
                    <p>Gather and return all category nodes and generated nodes with unique EItem combinations</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>        <span class="k">return</span> <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">getUniqueNodes</span><span class="p">();</span>
    <span class="p">}</span></pre></div>                </td>
            </tr>
                    <tr id="section-24">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-24">&#182;</a>
                    </div>
                    <p>======================================================================================</p>

<h3>generateNodes()</h3>

<hr />

<p>Recursive method that helps find all combinations of EItems</p>

<p><em class='docparam'>@param</em> <code>array() $node</code>
<em class='docparam'>@param</em> <code>int $count</code>
<em class='docparam'>@return</em> <code>void</code>
 ======================================================================================</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>    <span class="k">private</span> <span class="k">function</span> <span class="nf">generateNodes</span><span class="p">(</span><span class="nv">$node</span><span class="p">,</span> <span class="nv">$count</span><span class="p">)</span>
    <span class="p">{</span>
        <span class="k">for</span><span class="p">(</span><span class="nv">$j</span> <span class="o">=</span> <span class="nv">$count</span><span class="p">;</span> <span class="nv">$j</span> <span class="o">&lt;</span> <span class="nb">count</span><span class="p">(</span><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">Nodes</span><span class="p">);</span> <span class="nv">$j</span><span class="o">++</span><span class="p">)</span>
        <span class="p">{</span>
            <span class="nv">$tempNode</span> <span class="o">=</span> <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">merge</span><span class="p">(</span><span class="nv">$node</span><span class="p">,</span> <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">Nodes</span><span class="p">[</span><span class="nv">$j</span><span class="p">]);</span>

            <span class="k">if</span><span class="p">(</span><span class="nb">isset</span><span class="p">(</span><span class="nv">$tempNode</span><span class="p">))</span>
                <span class="nb">array_push</span><span class="p">(</span><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">nodeAcc</span><span class="p">,</span> <span class="nv">$tempNode</span><span class="p">);</span>

            <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">generateNodes</span><span class="p">(</span><span class="nv">$tempNode</span><span class="p">,</span> <span class="nv">$j</span> <span class="o">+</span> <span class="mi">1</span><span class="p">);</span>
        <span class="p">}</span>
    <span class="p">}</span></pre></div>                </td>
            </tr>
                    <tr id="section-25">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-25">&#182;</a>
                    </div>
                    <p>======================================================================================</p>

<h3>merge()</h3>

<hr />

<p>Helps the recursive method join together two nodes by seeing if they have any eItems
 in common.</p>

<p><em class='docparam'>@param</em> <code>array() $node1</code>
<em class='docparam'>@param</em> <code>array() $node2</code>
<em class='docparam'>@return</em> <code>array()</code>
 ======================================================================================</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>    <span class="k">private</span> <span class="k">function</span> <span class="nf">merge</span><span class="p">(</span><span class="nv">$node1</span><span class="p">,</span> <span class="nv">$node2</span><span class="p">)</span>
    <span class="p">{</span>
        <span class="nv">$tempNode</span> <span class="o">=</span> <span class="k">null</span><span class="p">;</span>

        <span class="k">if</span><span class="p">(</span><span class="nb">isset</span><span class="p">(</span><span class="nv">$node1</span><span class="p">)</span> <span class="o">&amp;&amp;</span> <span class="nb">isset</span><span class="p">(</span><span class="nv">$node2</span><span class="p">))</span>
        <span class="p">{</span></pre></div>                </td>
            </tr>
                    <tr id="section-26">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-26">&#182;</a>
                    </div>
                    <p>Grab eItemIDS from each node</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>            <span class="nv">$eItems1</span> <span class="o">=</span> <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">categoryEItems</span><span class="p">[</span><span class="nv">$node1</span><span class="p">[</span><span class="s1">&#39;Category ID&#39;</span><span class="p">]];</span>
            <span class="nv">$eItems2</span> <span class="o">=</span> <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">categoryEItems</span><span class="p">[</span><span class="nv">$node2</span><span class="p">[</span><span class="s1">&#39;Category ID&#39;</span><span class="p">]];</span></pre></div>                </td>
            </tr>
                    <tr id="section-27">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-27">&#182;</a>
                    </div>
                    <p>Find out which ones they have in common</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>            <span class="nv">$intersect</span> <span class="o">=</span> <span class="nb">array_intersect</span><span class="p">(</span><span class="nv">$eItems1</span><span class="p">,</span> <span class="nv">$eItems2</span><span class="p">);</span></pre></div>                </td>
            </tr>
                    <tr id="section-28">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-28">&#182;</a>
                    </div>
                    <p>Do they have anything in common?</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>            <span class="k">if</span><span class="p">(</span><span class="nb">count</span><span class="p">(</span><span class="nv">$intersect</span> <span class="o">&gt;</span> <span class="mi">0</span><span class="p">))</span>
            <span class="p">{</span></pre></div>                </td>
            </tr>
                    <tr id="section-29">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-29">&#182;</a>
                    </div>
                    <p>Don't include the recently generated nodes</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre></pre></div>                </td>
            </tr>
                    <tr id="section-30">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-30">&#182;</a>
                    </div>
                    <p>(if they are recently generated then they are not selected)</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>                <span class="k">if</span><span class="p">(</span><span class="nv">$node1</span><span class="p">[</span><span class="s1">&#39;Is Selected&#39;</span><span class="p">]</span> <span class="o">==</span> <span class="mi">1</span> <span class="o">&amp;&amp;</span> <span class="nv">$node2</span><span class="p">[</span><span class="s1">&#39;Is Selected&#39;</span><span class="p">]</span> <span class="o">==</span> <span class="mi">0</span><span class="p">)</span>
                <span class="p">{</span>
                    <span class="nv">$nodeIDs</span> <span class="o">=</span> <span class="k">array</span><span class="p">(</span><span class="nv">$node1</span><span class="p">[</span><span class="s1">&#39;Category ID&#39;</span><span class="p">]);</span>
                <span class="p">}</span>
                <span class="k">else</span> <span class="k">if</span><span class="p">(</span><span class="nv">$node1</span><span class="p">[</span><span class="s1">&#39;Is Selected&#39;</span><span class="p">]</span> <span class="o">==</span> <span class="mi">0</span> <span class="o">&amp;&amp;</span> <span class="nv">$node2</span><span class="p">[</span><span class="s1">&#39;Is Selected&#39;</span><span class="p">]</span> <span class="o">==</span> <span class="mi">1</span><span class="p">)</span>
                <span class="p">{</span>
                    <span class="nv">$nodeIDs</span> <span class="o">=</span> <span class="k">array</span><span class="p">(</span><span class="nv">$node2</span><span class="p">[</span><span class="s1">&#39;Category ID&#39;</span><span class="p">]);</span>
                <span class="p">}</span>
                <span class="k">else</span> <span class="k">if</span><span class="p">(</span><span class="nv">$node1</span><span class="p">[</span><span class="s1">&#39;Is Selected&#39;</span><span class="p">]</span> <span class="o">==</span> <span class="mi">0</span> <span class="o">&amp;&amp;</span> <span class="nv">$node2</span><span class="p">[</span><span class="s1">&#39;Is Selected&#39;</span><span class="p">]</span> <span class="o">==</span> <span class="mi">0</span><span class="p">)</span>
                <span class="p">{</span>
                    <span class="nv">$nodeIDs</span> <span class="o">=</span> <span class="k">array</span><span class="p">();</span>
                <span class="p">}</span>
                <span class="k">else</span>
                <span class="p">{</span>
                    <span class="nv">$nodeIDs</span> <span class="o">=</span> <span class="k">array</span><span class="p">(</span><span class="nv">$node1</span><span class="p">[</span><span class="s1">&#39;Category ID&#39;</span><span class="p">],</span> <span class="nv">$node2</span><span class="p">[</span><span class="s1">&#39;Category ID&#39;</span><span class="p">]);</span>
                <span class="p">}</span></pre></div>                </td>
            </tr>
                    <tr id="section-31">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-31">&#182;</a>
                    </div>
                    <p>Get all relevant, distinct category IDs together</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>                <span class="nv">$categories</span> <span class="o">=</span> <span class="nb">array_merge</span><span class="p">(</span><span class="nv">$node1</span><span class="p">[</span><span class="s1">&#39;Categories&#39;</span><span class="p">],</span> <span class="nv">$node2</span><span class="p">[</span><span class="s1">&#39;Categories&#39;</span><span class="p">]);</span>
                <span class="nv">$categories</span> <span class="o">=</span> <span class="nb">array_merge</span><span class="p">(</span><span class="nv">$categories</span><span class="p">,</span> <span class="nv">$nodeIDs</span><span class="p">);</span>
                <span class="nv">$categories</span> <span class="o">=</span> <span class="nb">array_unique</span><span class="p">(</span><span class="nv">$categories</span><span class="p">);</span></pre></div>                </td>
            </tr>
                    <tr id="section-32">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-32">&#182;</a>
                    </div>
                    <p>Create node ID by hashing unique EItem and Category arrays</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>                <span class="nv">$ID</span> <span class="o">=</span> <span class="nb">md5</span><span class="p">(</span><span class="nb">json_encode</span><span class="p">(</span><span class="nv">$intersect</span><span class="p">)</span><span class="o">.</span><span class="nb">json_encode</span><span class="p">(</span><span class="nv">$categories</span><span class="p">));</span></pre></div>                </td>
            </tr>
                    <tr id="section-33">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-33">&#182;</a>
                    </div>
                    <p>Create node if more categories</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>                <span class="nv">$tempNode</span> <span class="o">=</span> <span class="k">array</span>
                <span class="p">(</span>
                    <span class="s1">&#39;Is Generated&#39;</span> <span class="o">=&gt;</span> <span class="mi">1</span><span class="p">,</span>
                    <span class="s1">&#39;Is Selected&#39;</span> <span class="o">=&gt;</span> <span class="mi">0</span><span class="p">,</span>
                    <span class="s1">&#39;Category ID&#39;</span> <span class="o">=&gt;</span> <span class="nv">$ID</span><span class="p">,</span>
                    <span class="s1">&#39;Category String&#39;</span> <span class="o">=&gt;</span> <span class="nv">$node1</span><span class="p">[</span><span class="s1">&#39;Category String&#39;</span><span class="p">]</span> <span class="o">.</span> <span class="s1">&#39; ? &#39;</span> <span class="o">.</span> <span class="nv">$node2</span><span class="p">[</span><span class="s1">&#39;Category String&#39;</span><span class="p">],</span>
                    <span class="s1">&#39;Num EItems&#39;</span> <span class="o">=&gt;</span> <span class="nb">count</span><span class="p">(</span><span class="nv">$intersect</span><span class="p">),</span>
                    <span class="s1">&#39;EItems&#39;</span> <span class="o">=&gt;</span> <span class="nv">$intersect</span><span class="p">,</span>
                    <span class="s1">&#39;Categories&#39;</span> <span class="o">=&gt;</span> <span class="nv">$categories</span>
                <span class="p">);</span></pre></div>                </td>
            </tr>
                    <tr id="section-34">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-34">&#182;</a>
                    </div>
                    <p>Create EItems</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>                <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">categoryEItems</span><span class="p">[</span><span class="nv">$ID</span><span class="p">]</span> <span class="o">=</span> <span class="nv">$intersect</span><span class="p">;</span>
            <span class="p">}</span>
        <span class="p">}</span>

        <span class="k">return</span> <span class="nv">$tempNode</span><span class="p">;</span>
    <span class="p">}</span></pre></div>                </td>
            </tr>
                    <tr id="section-35">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-35">&#182;</a>
                    </div>
                    <p>======================================================================================</p>

<h3>addSelectedNodes()</h3>

<hr />

<p>Puts selected categories/generated nodes into useful data structure that will greatly
 aid in the generation of nodes.</p>

<p><em class='docparam'>@param</em> <code>array() $selectedCategories</code>
<em class='docparam'>@param</em> <code>array() $selectedGenerated</code>
<em class='docparam'>@return</em> <code>void</code>
 ======================================================================================</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>    <span class="k">private</span> <span class="k">function</span> <span class="nf">addSelectedNodes</span><span class="p">(</span><span class="nv">$selectedCategories</span><span class="p">,</span> <span class="nv">$selectedGenerated</span><span class="p">)</span>
    <span class="p">{</span></pre></div>                </td>
            </tr>
                    <tr id="section-36">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-36">&#182;</a>
                    </div>
                    <p>Add selected categories to nodes array</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>        <span class="k">foreach</span><span class="p">(</span><span class="nv">$selectedCategories</span> <span class="k">as</span> <span class="nv">$categoryID</span><span class="p">)</span>
        <span class="p">{</span></pre></div>                </td>
            </tr>
                    <tr id="section-37">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-37">&#182;</a>
                    </div>
                    <p>Get EItems and add them to global variable</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>            <span class="nv">$eItems</span> <span class="o">=</span> <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">getEItems</span><span class="p">(</span><span class="nv">$categoryID</span><span class="p">);</span>

            <span class="nv">$myCategory</span> <span class="o">=</span> <span class="nx">Category</span><span class="o">::</span><span class="na">find</span><span class="p">(</span><span class="nv">$categoryID</span><span class="p">);</span>

            <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">Nodes</span><span class="p">[]</span> <span class="o">=</span> <span class="k">array</span>
            <span class="p">(</span>
                <span class="s1">&#39;Is Generated&#39;</span> <span class="o">=&gt;</span> <span class="mi">0</span><span class="p">,</span>
                <span class="s1">&#39;Is Selected&#39;</span> <span class="o">=&gt;</span> <span class="mi">1</span><span class="p">,</span>
                <span class="s1">&#39;Category ID&#39;</span> <span class="o">=&gt;</span> <span class="nv">$categoryID</span><span class="p">,</span>
                <span class="s1">&#39;Category String&#39;</span> <span class="o">=&gt;</span> <span class="nv">$myCategory</span><span class="o">-&gt;</span><span class="p">{</span><span class="s1">&#39;Category String&#39;</span><span class="p">},</span>
                <span class="s1">&#39;Num EItems&#39;</span> <span class="o">=&gt;</span> <span class="nb">isset</span><span class="p">(</span><span class="nv">$eItems</span><span class="p">)</span> <span class="o">?</span> <span class="nb">count</span><span class="p">(</span><span class="nv">$eItems</span><span class="p">)</span> <span class="o">:</span> <span class="mi">0</span><span class="p">,</span>
                <span class="s1">&#39;EItems&#39;</span> <span class="o">=&gt;</span> <span class="nv">$eItems</span><span class="p">,</span>
                <span class="s1">&#39;Categories&#39;</span> <span class="o">=&gt;</span> <span class="k">array</span><span class="p">()</span>
            <span class="p">);</span>
        <span class="p">}</span></pre></div>                </td>
            </tr>
                    <tr id="section-38">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-38">&#182;</a>
                    </div>
                    <p>Add selected generated nodes to nodes array</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>        <span class="k">foreach</span><span class="p">(</span><span class="nv">$selectedGenerated</span> <span class="k">as</span> <span class="nv">$generatedNode</span><span class="p">)</span>
        <span class="p">{</span>
            <span class="nv">$categoryID</span> <span class="o">=</span> <span class="nv">$generatedNode</span><span class="p">[</span><span class="s1">&#39;Category ID&#39;</span><span class="p">];</span>
            <span class="nv">$EItems</span> <span class="o">=</span> <span class="nv">$generatedNode</span><span class="p">[</span><span class="s1">&#39;EItems&#39;</span><span class="p">];</span></pre></div>                </td>
            </tr>
                    <tr id="section-39">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-39">&#182;</a>
                    </div>
                    <p>Put EItems into global variable</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>            <span class="k">if</span><span class="p">(</span><span class="nb">isset</span><span class="p">(</span><span class="nv">$EItems</span><span class="p">)</span> <span class="o">&amp;&amp;</span> <span class="nb">isset</span><span class="p">(</span><span class="nv">$categoryID</span><span class="p">))</span>
            <span class="p">{</span>
                <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">categoryEItems</span><span class="p">[</span><span class="nv">$categoryID</span><span class="p">]</span> <span class="o">=</span> <span class="nv">$EItems</span><span class="p">;</span>
            <span class="p">}</span></pre></div>                </td>
            </tr>
                    <tr id="section-40">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-40">&#182;</a>
                    </div>
                    <p>Add node to array</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>            <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">Nodes</span><span class="p">[]</span> <span class="o">=</span> <span class="nv">$generatedNode</span><span class="p">;</span>
        <span class="p">}</span>
    <span class="p">}</span></pre></div>                </td>
            </tr>
                    <tr id="section-41">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-41">&#182;</a>
                    </div>
                    <p>======================================================================================</p>

<h3>getEItems()</h3>

<hr />

<p>Finds EItems for a given category (NOT generated), adds them to a global variable to
 help later node generation, and returns an array of EItems.</p>

<p><em class='docparam'>@param</em> <code>int $categoryID</code>
<em class='docparam'>@return</em> <code>array()</code>
 ======================================================================================</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>    <span class="k">private</span> <span class="k">function</span> <span class="nf">getEItems</span><span class="p">(</span><span class="nv">$categoryID</span><span class="p">)</span>
    <span class="p">{</span></pre></div>                </td>
            </tr>
                    <tr id="section-42">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-42">&#182;</a>
                    </div>
                    <p>Find EItem</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>        <span class="nv">$EItems</span> <span class="o">=</span> <span class="nx">CategoryEItem</span><span class="o">::</span><span class="na">where</span><span class="p">(</span><span class="s1">&#39;Category ID&#39;</span><span class="p">,</span> <span class="s1">&#39;=&#39;</span><span class="p">,</span> <span class="nv">$categoryID</span><span class="p">)</span><span class="o">-&gt;</span><span class="na">get</span><span class="p">();</span>

        <span class="nv">$eItemsArr</span> <span class="o">=</span> <span class="k">array</span><span class="p">();</span>

        <span class="k">if</span><span class="p">(</span><span class="nb">isset</span><span class="p">(</span><span class="nv">$EItems</span><span class="p">))</span>
        <span class="p">{</span></pre></div>                </td>
            </tr>
                    <tr id="section-43">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-43">&#182;</a>
                    </div>
                    <p>Grabs each EItem and puts it into an array</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>            <span class="k">foreach</span><span class="p">(</span><span class="nv">$EItems</span> <span class="k">as</span> <span class="nv">$eItem</span><span class="p">)</span>
            <span class="p">{</span>
                <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">categoryEItems</span><span class="p">[</span><span class="nv">$categoryID</span><span class="p">][]</span> <span class="o">=</span> <span class="nv">$eItem</span><span class="o">-&gt;</span><span class="p">{</span><span class="s1">&#39;E-item ID&#39;</span><span class="p">};</span>
                <span class="nv">$eItemsArr</span><span class="p">[]</span> <span class="o">=</span> <span class="nv">$eItem</span><span class="o">-&gt;</span><span class="p">{</span><span class="s1">&#39;E-item ID&#39;</span><span class="p">};</span>
            <span class="p">}</span>
        <span class="p">}</span>

        <span class="k">return</span> <span class="nv">$eItemsArr</span><span class="p">;</span>
    <span class="p">}</span></pre></div>                </td>
            </tr>
                    <tr id="section-44">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-44">&#182;</a>
                    </div>
                    <p>======================================================================================</p>

<h3>getUniqueNodes()</h3>

<hr />

<p>Finds generated nodes that have NOT been selected and only show the generated nodes,
 or intersections of categories, with the most categories.</p>

<p>The "most categories" condition is important because the node generation considers
 ALL combinations of EItems and Categories. We ONLY want the UNIQUE EITEM combinations
 (so only ONE generated node for a combination of EItems) that has the MOST categories.
 This ensures that we do not see extraneous combinations that are useless to the user.</p>

<p><em class='docparam'>@return</em> <code>JSON</code>
 ======================================================================================</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>    <span class="k">private</span> <span class="k">function</span> <span class="nf">getUniqueNodes</span><span class="p">()</span>
    <span class="p">{</span>
        <span class="nv">$generatedCombinations</span> <span class="o">=</span> <span class="k">array</span><span class="p">();</span>
        <span class="nv">$finalNodes</span> <span class="o">=</span> <span class="k">array</span><span class="p">();</span></pre></div>                </td>
            </tr>
                    <tr id="section-45">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-45">&#182;</a>
                    </div>
                    <p>Get the nodes along with the generated nodes - separate them out for later sorting</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>        <span class="k">foreach</span><span class="p">(</span><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">nodeAcc</span> <span class="k">as</span> <span class="nv">$item</span><span class="p">)</span>
        <span class="p">{</span></pre></div>                </td>
            </tr>
                    <tr id="section-46">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-46">&#182;</a>
                    </div>
                    <p>Make each item a stdClass object</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>            <span class="nv">$item</span> <span class="o">=</span> <span class="p">(</span><span class="nx">object</span><span class="p">)</span><span class="nv">$item</span><span class="p">;</span></pre></div>                </td>
            </tr>
                    <tr id="section-47">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-47">&#182;</a>
                    </div>
                    <p>If it's generated and not selected, hash the eItems to index them by EItem</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>            <span class="k">if</span><span class="p">(</span><span class="nv">$item</span><span class="o">-&gt;</span><span class="p">{</span><span class="s2">&quot;Is Generated&quot;</span><span class="p">}</span> <span class="o">==</span> <span class="mi">1</span> <span class="o">&amp;&amp;</span> <span class="nv">$item</span><span class="o">-&gt;</span><span class="p">{</span><span class="s2">&quot;Is Selected&quot;</span><span class="p">}</span> <span class="o">==</span> <span class="mi">0</span><span class="p">)</span>
            <span class="p">{</span>
                <span class="nv">$generatedCombinations</span><span class="p">[</span><span class="nb">md5</span><span class="p">(</span><span class="nb">json_encode</span><span class="p">(</span><span class="nv">$item</span><span class="o">-&gt;</span><span class="na">EItems</span><span class="p">))][</span><span class="nb">count</span><span class="p">(</span><span class="nv">$item</span><span class="o">-&gt;</span><span class="na">Categories</span><span class="p">)][]</span> <span class="o">=</span> <span class="nv">$item</span><span class="p">;</span>
            <span class="p">}</span></pre></div>                </td>
            </tr>
                    <tr id="section-48">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-48">&#182;</a>
                    </div>
                    <p>Else, just add it on to the array</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>            <span class="k">else</span>
            <span class="p">{</span>
                <span class="nv">$finalNodes</span><span class="p">[]</span> <span class="o">=</span> <span class="nv">$item</span><span class="p">;</span>
            <span class="p">}</span>
        <span class="p">}</span></pre></div>                </td>
            </tr>
                    <tr id="section-49">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-49">&#182;</a>
                    </div>
                    <p>Find the generated node with the most categories and add it to the final output</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>        <span class="k">foreach</span><span class="p">(</span><span class="nv">$generatedCombinations</span> <span class="k">as</span> <span class="nv">$itemset</span><span class="p">)</span>
        <span class="p">{</span></pre></div>                </td>
            </tr>
                    <tr id="section-50">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-50">&#182;</a>
                    </div>
                    <p>Sort based on count of categories</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>            <span class="nb">krsort</span><span class="p">(</span><span class="nv">$itemset</span><span class="p">);</span></pre></div>                </td>
            </tr>
                    <tr id="section-51">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-51">&#182;</a>
                    </div>
                    <p>Remove count values as indexes</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>            <span class="nv">$itemset</span> <span class="o">=</span> <span class="nb">array_values</span><span class="p">(</span><span class="nv">$itemset</span><span class="p">);</span></pre></div>                </td>
            </tr>
                    <tr id="section-52">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-52">&#182;</a>
                    </div>
                    <p>Get the highest count of categories</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>            <span class="nv">$itemset</span> <span class="o">=</span> <span class="nb">array_values</span><span class="p">(</span><span class="nv">$itemset</span><span class="p">[</span><span class="mi">0</span><span class="p">]);</span></pre></div>                </td>
            </tr>
                    <tr id="section-53">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-53">&#182;</a>
                    </div>
                    <p>Merge the highest count array with the output array</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>            <span class="nv">$finalNodes</span> <span class="o">=</span> <span class="nb">array_merge</span><span class="p">(</span><span class="nv">$finalNodes</span><span class="p">,</span> <span class="nv">$itemset</span><span class="p">);</span>
        <span class="p">}</span>

        <span class="k">return</span> <span class="nb">json_encode</span><span class="p">(</span><span class="nv">$finalNodes</span><span class="p">);</span>
    <span class="p">}</span></pre></div>                </td>
            </tr>
                    <tr id="section-54">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-54">&#182;</a>
                    </div>
                    <p>////////////////////////////////////////////////////////////////////////////////////////////////</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre><span class="p">}</span>
</pre></div></pre></div>                </td>
            </tr>
                </tbody>
    </table>
</div>

@stop