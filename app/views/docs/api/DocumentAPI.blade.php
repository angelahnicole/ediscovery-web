@extends('docs.master')

@include('docs._partials.navigation')

@include('docs._partials.footer')

@section('content')

<div id="container">
    <div id="background"></div>
                    <div id="jump_to">
            Jump To &hellip;
            <div id="jump_wrapper">
                <div id="jump_page">
                    <ul>
                                                                                                                        <li></li>                                                <li><a class="source level_0" href="http://192.81.129.120:8080/docs/api/DocumentAPI">
                                                                        DocumentAPI.php                                    </a></li>
                                                                                                                                <li></li>                                                <li><a class="source level_0" href="http://192.81.129.120:8080/docs/api/CategoryAPI">
                                                                        CategoryAPI.php                                    </a></li>
                                                                                                                                <li></li>                                                <li><a class="source level_0" href="http://192.81.129.120:8080/docs/api/EItemAPI">
                                                                        EItemAPI.php                                    </a></li>
                                                    </ul>
                </div>
            </div>
        </div>
        <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th class="docs">
                <h1>
                    DocumentAPI.php                </h1>
            </th>
            <th class="code">
            </th>
        </tr>
        </thead>
        <tbody>
                    <tr id="section-0">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-0">&#182;</a>
                    </div>
                    <p>===================================================================================================
 DocumentAPI.php</p>

<hr />

<p>eDiscovery Web Application</p>

<p><a href="https://bitbucket.org/umadvprog">Advanced Programming Team</a></p>

<p><em class='docparam'>@authors</em> Angela Gross, Scott Halstvedt</p>

<p>This API combines Documents, Email Items, and FileTable tables into one interface. The phrase
 Documents" refers to all three of those tables loosely, but mostly refers to the FileTable
 rows.
 ===================================================================================================</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre><span class="k">class</span> <span class="nc">DocumentAPI</span> <span class="k">extends</span> <span class="nx">BaseController</span>
<span class="p">{</span></pre></div>                </td>
            </tr>
                    <tr id="section-1">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-1">&#182;</a>
                    </div>
                    <p>////////////////////////////////////////////////////////////////////////////////////////////////</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre></pre></div>                </td>
            </tr>
                    <tr id="section-2">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-2">&#182;</a>
                    </div>
                    <p>======================================================================================</p>

<h3>documents()</h3>

<hr />

<p>Grabs all files. This makes the generalization of using documents and email items and
 grabbing them all from the file-table. If an ID is provided, then it finds the related
 FILETABLE entry.</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre></pre></div>                </td>
            </tr>
                    <tr id="section-3">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-3">&#182;</a>
                    </div>
                    <p><em class='docparam'>@param</em> <code>int $documentID</code></p>
                </td>
                <td class="code">
                    <div class="highlight"><pre></pre></div>                </td>
            </tr>
                    <tr id="section-4">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-4">&#182;</a>
                    </div>
                    <p><em class='docparam'>@return</em> <code>JSON</code></p>
                </td>
                <td class="code">
                    <div class="highlight"><pre></pre></div>                </td>
            </tr>
                    <tr id="section-5">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-5">&#182;</a>
                    </div>
                    <p>======================================================================================</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>    <span class="k">public</span> <span class="k">function</span> <span class="nf">documents</span><span class="p">(</span><span class="nv">$documentID</span> <span class="o">=</span> <span class="k">null</span><span class="p">)</span>
    <span class="p">{</span></pre></div>                </td>
            </tr>
                    <tr id="section-6">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-6">&#182;</a>
                    </div>
                    <p>If there is no id, then return all documents in JSON format</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>        <span class="k">if</span> <span class="p">(</span><span class="nb">is_null</span><span class="p">(</span><span class="nv">$documentID</span><span class="p">))</span>
        <span class="p">{</span>
            <span class="nv">$allDocuments</span> <span class="o">=</span> <span class="k">array</span><span class="p">();</span>

            <span class="k">foreach</span> <span class="p">(</span><span class="nx">FileTable</span><span class="o">::</span><span class="na">all</span><span class="p">()</span> <span class="k">as</span> <span class="nv">$file</span><span class="p">)</span>
            <span class="p">{</span></pre></div>                </td>
            </tr>
                    <tr id="section-7">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-7">&#182;</a>
                    </div>
                    <p>Reset temp arrays</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>                <span class="nv">$tempEmails</span> <span class="o">=</span> <span class="k">array</span><span class="p">();</span>
                <span class="nv">$tempDocuments</span> <span class="o">=</span> <span class="k">array</span><span class="p">();</span></pre></div>                </td>
            </tr>
                    <tr id="section-8">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-8">&#182;</a>
                    </div>
                    <p>Get associated emails documents</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>                <span class="nv">$emails</span> <span class="o">=</span> <span class="nv">$file</span><span class="o">-&gt;</span><span class="na">Emails</span><span class="p">();</span>
                <span class="nv">$documents</span> <span class="o">=</span> <span class="nv">$file</span><span class="o">-&gt;</span><span class="na">Documents</span><span class="p">();</span></pre></div>                </td>
            </tr>
                    <tr id="section-9">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-9">&#182;</a>
                    </div>
                    <p>Format each email (if there are any)</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>                <span class="k">foreach</span><span class="p">(</span><span class="nv">$emails</span> <span class="k">as</span> <span class="nv">$email</span><span class="p">)</span>
                <span class="p">{</span>
                    <span class="nv">$tempEmails</span><span class="p">[]</span> <span class="o">=</span> <span class="k">array</span>
                    <span class="p">(</span>
                        <span class="s1">&#39;Email ID&#39;</span> <span class="o">=&gt;</span> <span class="nv">$email</span><span class="o">-&gt;</span><span class="p">{</span><span class="s1">&#39;Email ID&#39;</span><span class="p">},</span>
                        <span class="s1">&#39;E-item ID&#39;</span> <span class="o">=&gt;</span> <span class="nv">$email</span><span class="o">-&gt;</span><span class="p">{</span><span class="s1">&#39;E-item ID&#39;</span><span class="p">},</span>
                        <span class="s1">&#39;To&#39;</span> <span class="o">=&gt;</span> <span class="nv">$email</span><span class="o">-&gt;</span><span class="p">{</span><span class="s1">&#39;To&#39;</span><span class="p">},</span>
                        <span class="s1">&#39;Sender&#39;</span> <span class="o">=&gt;</span> <span class="nv">$email</span><span class="o">-&gt;</span><span class="p">{</span><span class="s1">&#39;Sender&#39;</span><span class="p">},</span>
                        <span class="s1">&#39;Date Sent&#39;</span> <span class="o">=&gt;</span> <span class="nv">$email</span><span class="o">-&gt;</span><span class="p">{</span><span class="s1">&#39;Date Sent&#39;</span><span class="p">},</span>
                        <span class="s1">&#39;Subject&#39;</span> <span class="o">=&gt;</span> <span class="nv">$email</span><span class="o">-&gt;</span><span class="p">{</span><span class="s1">&#39;Subject&#39;</span><span class="p">}</span>
                    <span class="p">);</span>
                <span class="p">}</span></pre></div>                </td>
            </tr>
                    <tr id="section-10">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-10">&#182;</a>
                    </div>
                    <p>Format each document (if there are any)</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>                <span class="k">foreach</span><span class="p">(</span><span class="nv">$documents</span> <span class="k">as</span> <span class="nv">$document</span><span class="p">)</span>
                <span class="p">{</span>
                    <span class="nv">$tempDocuments</span><span class="p">[]</span> <span class="o">=</span> <span class="k">array</span>
                    <span class="p">(</span>
                        <span class="s1">&#39;Document ID&#39;</span> <span class="o">=&gt;</span> <span class="nv">$document</span><span class="o">-&gt;</span><span class="p">{</span><span class="s1">&#39;Document ID&#39;</span><span class="p">},</span>
                        <span class="s1">&#39;E-item ID&#39;</span> <span class="o">=&gt;</span> <span class="nv">$document</span><span class="o">-&gt;</span><span class="p">{</span><span class="s1">&#39;E-item ID&#39;</span><span class="p">}</span>
                    <span class="p">);</span>
                <span class="p">}</span></pre></div>                </td>
            </tr>
                    <tr id="section-11">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-11">&#182;</a>
                    </div>
                    <p>Add specific file information</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>                <span class="nv">$allDocuments</span><span class="p">[]</span> <span class="o">=</span> <span class="k">array</span>
                <span class="p">(</span>
                    <span class="s1">&#39;File Name&#39;</span> <span class="o">=&gt;</span> <span class="nv">$file</span><span class="o">-&gt;</span><span class="p">{</span><span class="s1">&#39;File Name&#39;</span><span class="p">},</span>
                    <span class="s1">&#39;File Path&#39;</span> <span class="o">=&gt;</span> <span class="nv">$file</span><span class="o">-&gt;</span><span class="p">{</span><span class="s1">&#39;File Path&#39;</span><span class="p">},</span>
                    <span class="s1">&#39;File Type&#39;</span> <span class="o">=&gt;</span> <span class="nv">$file</span><span class="o">-&gt;</span><span class="p">{</span><span class="s1">&#39;File Type&#39;</span><span class="p">},</span>
                    <span class="s1">&#39;Email Items&#39;</span> <span class="o">=&gt;</span> <span class="nv">$tempEmails</span><span class="p">,</span>
                    <span class="s1">&#39;Documents&#39;</span> <span class="o">=&gt;</span> <span class="nv">$tempDocuments</span>
                <span class="p">);</span>
            <span class="p">}</span>

            <span class="k">return</span> <span class="nx">Response</span><span class="o">::</span><span class="na">make</span><span class="p">(</span><span class="nb">json_encode</span><span class="p">(</span><span class="nv">$allDocuments</span><span class="p">),</span> <span class="mi">200</span><span class="p">,</span> <span class="k">array</span><span class="p">(</span><span class="s1">&#39;Content-Type&#39;</span> <span class="o">=&gt;</span> <span class="s1">&#39;application/json&#39;</span><span class="p">));</span>
        <span class="p">}</span></pre></div>                </td>
            </tr>
                    <tr id="section-12">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-12">&#182;</a>
                    </div>
                    <p>Try to find document with given ID</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>        <span class="k">else</span>
        <span class="p">{</span>
            <span class="nv">$file</span> <span class="o">=</span> <span class="nx">FileTable</span><span class="o">::</span><span class="na">find</span><span class="p">(</span><span class="nv">$documentID</span><span class="p">);</span></pre></div>                </td>
            </tr>
                    <tr id="section-13">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-13">&#182;</a>
                    </div>
                    <p>If it is null, then tell user.</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>            <span class="k">if</span> <span class="p">(</span><span class="nb">is_null</span><span class="p">(</span><span class="nv">$file</span><span class="p">))</span>
            <span class="p">{</span>
                <span class="k">return</span> <span class="nx">Response</span><span class="o">::</span><span class="na">json</span><span class="p">(</span><span class="s1">&#39;Document not found&#39;</span><span class="p">,</span> <span class="mi">404</span><span class="p">);</span>
            <span class="p">}</span></pre></div>                </td>
            </tr>
                    <tr id="section-14">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-14">&#182;</a>
                    </div>
                    <p>Else, return it to user.</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>            <span class="k">else</span>
            <span class="p">{</span>
                <span class="k">return</span> <span class="nv">$file</span><span class="p">;</span>
            <span class="p">}</span>
        <span class="p">}</span>
    <span class="p">}</span></pre></div>                </td>
            </tr>
                    <tr id="section-15">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-15">&#182;</a>
                    </div>
                    <p>======================================================================================</p>

<h3>search()</h3>

<hr />

<p>Apply a plain text search against file names and file paths.</p>

<p><em class='docparam'>@param</em> <code>string $term</code>
<em class='docparam'>@return</em> <code>JSON</code>
 ====================================================================================== </p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>    <span class="k">public</span> <span class="k">function</span> <span class="nf">search</span><span class="p">(</span><span class="nv">$term</span> <span class="o">=</span> <span class="k">null</span><span class="p">)</span>
    <span class="p">{</span>
        <span class="k">if</span><span class="p">(</span><span class="nb">is_null</span><span class="p">(</span><span class="nv">$term</span><span class="p">))</span>
        <span class="p">{</span>
            <span class="k">return</span> <span class="nx">Response</span><span class="o">::</span><span class="na">json</span><span class="p">(</span><span class="s1">&#39;Search term required&#39;</span><span class="p">,</span> <span class="mi">400</span><span class="p">);</span>
        <span class="p">}</span>
        <span class="k">else</span>
        <span class="p">{</span>
            <span class="k">return</span> <span class="nx">FileTable</span><span class="o">::</span><span class="na">where</span><span class="p">(</span><span class="s1">&#39;File Name&#39;</span><span class="p">,</span> <span class="s1">&#39;LIKE&#39;</span><span class="p">,</span> <span class="s2">&quot;%</span><span class="si">$term</span><span class="s2">%&quot;</span><span class="p">)</span><span class="o">-&gt;</span><span class="na">orWhere</span><span class="p">(</span><span class="s1">&#39;File Path&#39;</span><span class="p">,</span> <span class="s1">&#39;LIKE&#39;</span><span class="p">,</span> <span class="s2">&quot;%</span><span class="si">$term</span><span class="s2">%&quot;</span><span class="p">)</span><span class="o">-&gt;</span><span class="na">get</span><span class="p">();</span>
        <span class="p">}</span>
    <span class="p">}</span></pre></div>                </td>
            </tr>
                    <tr id="section-16">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-16">&#182;</a>
                    </div>
                    <p>////////////////////////////////////////////////////////////////////////////////////////////////</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre><span class="p">}</span>
</pre></div></pre></div>                </td>
            </tr>
                </tbody>
    </table>
</div>

@stop