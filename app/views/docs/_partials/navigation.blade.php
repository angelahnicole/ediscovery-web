{{-- NAVIGATION --}}
@section('navigation')

<nav class="navbar navbar-inverse" role="navigation" style="margin-bottom:0px">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ URL::to('docs') }}">eDiscovery</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="{{ Request::is('docs') ? 'active' : null }}"><a href="{{ URL::to('docs') }}">Home</a></li>
                <li class="dropdown {{ Request::is('docs/models*') || Request::is('docs/API*') || Request::is('docs/routes') ? 'active' : null }}">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Backend <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ URL::to('docs/API') }}">API</a></li>
                        <li><a href="{{ URL::to('docs/models') }}">Models</a></li>
                        <li><a href="{{ URL::to('docs/routes') }}">Routes</a></li>
                    </ul>
                </li>
            </ul>


        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

@stop