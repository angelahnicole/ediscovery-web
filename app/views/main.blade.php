<!DOCTYPE html>
<html lang="en">
<head>
    <title>eDiscovery</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
	
    {{ HTML::style('css/main.css'); }}
    {{ HTML::style('css/info.css'); }}
    {{ HTML::style('css/sidebar.css'); }}
    {{ HTML::script('js/three.min.js'); }}
    {{ HTML::script('js/three.js'); }}
    {{ HTML::script('js/TrackballControls.js'); }}
    {{ HTML::script('js/stats.min.js'); }}
    {{ HTML::script('js/tween.min.js'); }}
    {{ HTML::script('js/jquery-1.11.0.js'); }}
    {{ HTML::script('js/underscore.js'); }}
    {{ HTML::script('js/backbone-min.js'); }}
    {{ HTML::script('js/handlebars-v1.3.0.js'); }}
    {{ HTML::script('js/checkbox-field.js'); }}
    {{ HTML::script('js/sphere.js'); }}
    {{ HTML::script('js/meny.js'); }}
    {{ HTML::script('js/springy.js'); }}
    {{ HTML::script('js/springyui.js'); }}
    {{ HTML::script('js/refined.js'); }}
    {{ HTML::script('js/jquery-1.11.0.js'); }}
    {{ HTML::script('js/CSS3DRenderer.js'); }}
    {{ HTML::script('js/sidebar.js'); }}
	
	
</head>
<body>
    
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
{{ HTML::script('js/info.js'); }}	
<script type="text/javascript"></script>
<div id="infoButton">
    <div id="infoBox" title="Help">
         <div class="cat_info">
			<div class="cat_img"></div>
			<div class="cat_text">The many multi-colored Spheres you see will each be a category you have selected.</div>
		 </div>
         <div class="cat_info">
			<div class="un_img"></div>
			<div class="cat_text">The Gold Spheres you see are the unions associated with the categories.</div>
		</div>
    </div>
</div>

{{ HTML::script('js/main.js'); }}

<script type="text/javascript">
  createView();
</script>

<div class="meny-arrow"></div>
<div class="content">
</div>

<script type="text/javascript">
    /*var meny = Meny.create({
    // The element that will be animated in from off screen
    menuElement: document.querySelector( '.menu_wrapper' ),

    // The contents that gets pushed aside while Meny is active
    contentsElement: document.querySelector( '.content' ),

    // [optional] The alignment of the menu (top/right/bottom/left)
    position: Meny.getQuery().p || 'right',

    // [optional] The height of the menu (when using top/bottom position)
    height: 200,

    // [optional] The width of the menu (when using left/right position)
    width: 380,

    // [optional] Distance from mouse (in pixels) when menu should open
    threshold: 160,

    // [optional] Use mouse movement to automatically open/close
    mouse: true,

    // [optional] Use touch swipe events to open/close
    touch: true
    });
	
	 // Embed an iframe if a URL is passed in
    if( Meny.getQuery().u && Meny.getQuery().u.match( /^http/gi ) ) {
    var contents = document.querySelector( '.contents' );
    contents.style.padding = '0px';
    contents.innerHTML = '<div class="cover"></div><iframe src="'+ Meny.getQuery().u +'" style="width: 100%; height: 100%; border: 0; position: absolute;"></iframe>';
    }

    meny.open();*/
</script>

</body>
</html>
