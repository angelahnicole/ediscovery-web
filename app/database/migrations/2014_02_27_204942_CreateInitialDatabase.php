<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

// =======================================================================================
// CreateInitialDatabase.php
// ---------------------------------------------------------------------------------------
// eDiscovery Web Application
// Advanced Programming Team [https://bitbucket.org/umadvprog]
// @authors Angela Gross, Scott Halstvedt
// ---------------------------------------------------------------------------------------
//
// =======================================================================================

class CreateInitialDatabase extends Migration
{
    //////////////////////////////////////////////////////////////////////////////////////

    // ===================================================================================
    // RUN MIGRATIONS
    // ===================================================================================
    public function up()
    {
        // -------------------------------------------------------------------------------
        // CATEGORY -> Drop table and create it
        // -------------------------------------------------------------------------------
        Schema::dropIfExists('CategoryData.[Category]');
        Schema::create('CategoryData.[Category]', function (Blueprint $table)
        {
            $table->increments('Category ID');
            $table->string('Category String');
            $table->integer('E-item Count');
        });

        // -------------------------------------------------------------------------------
        // EITEM -> Drop table and create it
        // -------------------------------------------------------------------------------
        Schema::dropIfExists('EitemData.[E-item]');
        Schema::create('EitemData.[E-item]', function (Blueprint $table)
        {
            $table->increments('E-item ID');
        });

        // -------------------------------------------------------------------------------
        // CATEGORY-E-ITEM (join) -> Drop table and create it
        // -------------------------------------------------------------------------------
        Schema::dropIfExists('CategoryData.[Category-E-item]');
        Schema::create('CategoryData.[Category-E-item]', function (Blueprint $table)
        {
            $table->increments('Category Item ID');
            $table->foreign('Category ID')->references('Category ID')->on('CategoryData.[Category]');
            $table->foreign('E-item ID')->references('E-item ID')->on('EitemData.[E-item]');

        });

        // -------------------------------------------------------------------------------
        // FILETABLE -> Drop table and create it
        // -------------------------------------------------------------------------------
        Schema::dropIfExists('ProjectData.FileTable');
        Schema::create('ProjectData.FileTable', function (Blueprint $table)
        {
            $table->increments('File ID');
            $table->string('File Name');
            $table->string('File Type');
        });

        // -------------------------------------------------------------------------------
        // DOCUMENT (join) -> Drop table and create it
        // -------------------------------------------------------------------------------
        Schema::dropIfExists('EitemData.[Document]');
        Schema::create('EitemData.[Document]', function (Blueprint $table)
        {
            $table->increments('Document ID');
            $table->foreign('E-item ID')->references('E-item ID')->on('EitemData.[E-item]');
            $table->foreign('File ID')->references('File ID')->on('ProjectData.FileTable');
        });


        // -------------------------------------------------------------------------------
        // EMAIL ITEM (join) -> Drop table and create it
        // -------------------------------------------------------------------------------
        Schema::dropIfExists('EitemData.[Email Item]');
        Schema::create('EitemData.[Email Item]', function (Blueprint $table)
        {
            $table->increments('Email ID');
            $table->foreign('E-item ID')->references('E-item ID')->on('EitemData.[E-item]');
            $table->foreign('File ID')->references('File ID')->on('ProjectData.FileTable');
        });
    }

    //////////////////////////////////////////////////////////////////////////////////////

    // ===================================================================================
    // REVERSE MIGRATIONS
    // -----------------------------------------------------------------------------------
    // Drops all the tables in the proper order to avoid an integrity constraint violation
    // ===================================================================================
    public function down()
    {
        Schema::dropIfExists('CategoryData.[Category-E-item]');
        Schema::dropIfExists('CategoryData.[Category]');
        Schema::dropIfExists('EitemData.[Document]');
        Schema::dropIfExists('EitemData.[Email Item]');
        Schema::dropIfExists('ProjectData.FileTable');
        Schema::dropIfExists('EitemData.[E-item]');
    }

    //////////////////////////////////////////////////////////////////////////////////////
}